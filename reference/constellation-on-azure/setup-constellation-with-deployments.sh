#!/bin/bash

# Check if required tools are installed and available in PATH
if [[ ! $(type -P "kubectl") ]]; then
    echo "kubectl has not been found in your PATH. Please make sure it is installed it before running this script."
    prerequisites_error=true
fi

if [[ ! $(type -P "helm") ]]; then
    echo "Helm has not been found in your PATH. Please make sure it is installed it before running this script."
    prerequisites_error=true
fi

if [[ ! $(type -P "constellation") ]]; then
    echo "The Constellation CLI has not been found in your PATH. Please make sure it is installed it before running this script."
    prerequisites_error=true
fi

# We expect that RESOURCE_GROUP and LOCATION are already defined outside the script (see README.md)
if [[ -z "$RESOURCE_GROUP" ]]; then
  echo "RESOURCE_GROUP has not been defined or exported as an environment variable."
  prerequisites_error=true
fi

if [[ -z "$LOCATION" ]]; then
  echo "LOCATION has not been defined or exported as an environment variable."
  prerequisites_error=true
fi

if [[ "$prerequisites_error" = true ]]; then
  exit 1
fi

# Settings specifically for ReadWriteMany storage
STORAGE_ACCOUNT_NAME="havendemoazurefiles"
DISK_SKU="Standard_LRS" # Don't use Premium, it will break the test. Premium disks require you to allocate a minimum of 100 GB in a PVC on Azure Files, and the test uses significantly less.

# Create Constellation cluster with three control plane nodes and three worker nodes
set -euxo pipefail
constellation create -c 3 -w 3 --yes
constellation init --conformance
export KUBECONFIG="$PWD/constellation-admin.conf"

# Setup confidential ReadWriteOnce storage using Constellation CSI driver
helm install azuredisk-csi-driver https://raw.githubusercontent.com/edgelesssys/constellation-azuredisk-csi-driver/main/charts/edgeless/v1.0.1/azuredisk-csi-driver.tgz \
    --namespace kube-system \
    --set linux.distro=fedora \
    --set controller.replicas=1
cat <<EOF | kubectl apply -f -
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: encrypted-storage
provisioner: azuredisk.csi.confidential.cloud
parameters:
  skuName: Standard_LRS
reclaimPolicy: Delete
volumeBindingMode: WaitForFirstConsumer
EOF

# Setup ReadWriteMany storage using Azure Files
# Note: This breaks "constellation terminate" currently (v2.1.0) as the storage account in the same resource group as the cluster breaks the regexp error handling logic in the CLI.
# To fix this, clear out all resources in the resource group manually and then run "constellation terminate" again.
# This should be fixed in future versions.
helm repo add azurefile-csi-driver https://raw.githubusercontent.com/kubernetes-sigs/azurefile-csi-driver/master/charts
helm install azurefile-csi-driver azurefile-csi-driver/azurefile-csi-driver --namespace kube-system --set linux.distro=fedora --version 1.22.0
az storage account create -n $STORAGE_ACCOUNT_NAME -g $RESOURCE_GROUP -l $LOCATION --sku $DISK_SKU --kind StorageV2 --enable-large-file-share
STORAGE_ACCOUNT_KEY=$(az storage account keys list -g $RESOURCE_GROUP -n $STORAGE_ACCOUNT_NAME -o json | jq -r ".[0].value")
kubectl create secret generic azure-secret --from-literal azurestorageaccountname=$STORAGE_ACCOUNT_NAME --from-literal azurestorageaccountkey="$STORAGE_ACCOUNT_KEY" --type=Opaque
kubectl create -f https://raw.githubusercontent.com/kubernetes-sigs/azurefile-csi-driver/v1.22.0/deploy/example/storageclass-azurefile-secret.yaml

# Install cert-manager (not configured in any special way)
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.9.1/cert-manager.yaml

# Install Prometheus with Node Exporter and kube-state-metrics
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm upgrade --install prometheus-stack -f helm/prometheus-stack-values.yaml prometheus-community/kube-prometheus-stack --namespace=monitoring --create-namespace

# Install Loki + Promtail
helm upgrade --install loki grafana/loki-stack --namespace=monitoring

# Install Grafana with Loki & Prometheus as data sources preconfigured
helm upgrade --install grafana grafana/grafana -f helm/grafana-values.yaml --namespace=monitoring

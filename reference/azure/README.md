# Reference Implementation: Haven on Azure

## AKS
Haven can easily be deployed on Azure using [AKS](https://docs.microsoft.com/azure/aks).

## How it works
Follow the steps from the [online documentation](https://haven.commonground.nl/techniek/aan-de-slag/azure) or have a look at the [terraform code](./terraform).

## License
Copyright © VNG Realisatie 2019-2023
Licensed under the EUPL

# AKS Haven Environment

The environment is an infrastructure + tools which do not directly affect the applications to be hosted but necessary for operations and compliance check.

## Before You Start

Make sure you have all sufficient privileges in your Azure environment (Contributor role at least) and required tools with the appropriate version installed:

* azure-cli >= 2.24.2
* terraform >= 0.15.5
* kubectl >= 1.20.2

## Greenfield Reference Implementation

This reference implementation is designed for a greenfield deployment which means:

1. There is no pre-defined networking infrastructure
2. AKS uses system-assigned managed identity which is created during cluster creation

For brownfield implementation, where, for example, networking has been given and permissions are restricted, please consider [bring your own managed identity](https://docs.microsoft.com/en-us/azure/aks/use-managed-identity#bring-your-own-control-plane-mi).

## Preparations: Create AAD AKS Admins Group and Describe Your Environment

### This Reference Implementation Is Based on AKS with AAD Integration

1. Create an AKS admin group in your AAD

```bash
aksAdminGroupName="HavenAksAdmins"
az ad group create --display-name ${aksAdminGroupName} --mail-nickname ${aksAdminGroupName}
```

2. Define the Haven AKS admin user(s) and get userID(s)

```bash
aksAdminUserId = $(az ad user list --filter  "displayname eq 'John Doe'"  --query '[0].objectId')
```

3. Add AKS admin user(s) to the AKS Admin group

```bash
az ad group member add --group ${aksAdminGroupName} --member-id ${aksAdminUserId}
```

### Describe your desired cluster as a set of variables

Clone the repository and adjust [AKS infrastructure](./terraform/infrastructure/variables.tf) and [AKS tooling](./terraform/tools/variables.tf) variables.

#### CRUD AKS Nodepools

The code design in a way that allows to add, delete, update the number of nodepools before and after cluster creation.

Make your changes under `additional_node_pools`:

```bash
...
      nodepool2 = {
        name                           = "nodepool2"
        node_count                     = 0
        node_os                        = "Linux"
        vm_size                        = "Standard_D4_v3"
        os_disk_size_gb                = 128
        max_pods                       = 250
        node_public_ip_enabled          = false
        zones                          = ["1", "2", "3"]
        taints                         = null
        cluster_auto_scaling           = true
        cluster_auto_scaling_min_count = 0
        cluster_auto_scaling_max_count = 5
        labels = {
          "pool_name" = "node_pool_2"
          "label_1"   = "value_1"
        }
      }
...
```

#### Log Analytics integration

Integrate your cluster with Azure Monitor. Under `addons` section set `oms_agent.enabled` to `true` and specify `log_analytics_workspace_id`

```bash
...
    addons = {
      oms_agent   = {
        enabled = false # if true, put the right analytics_workspace_id. see example below. if false, leave analytics_workspace_id as described below
        log_analytics_workspace_id = "/subscriptions/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx/resourceGroups/rg-name/providers/Microsoft.OperationalInsights/workspaces/workspace-name"
      }
      kubernetes_dashboard = false
      azure_policy         = false
    }
...
```

#### ACR integration

Integrate your cluster with Azure Container Registry(ACR)

```bash
...
  container_registry = {
    enabled = false # if true, put the right container registry id to create 'pull' role assignement. if false, leave id as described below
    id = "aksacr"
  }
...
```

## Rolling Out

### Infrastructure

Infrastructure creates Resource Group, Virtual Network with Subnet and AKS cluster.
Specify the name of the AKS admin group that you have created and roll out an AKS cluster.

```bash
export TF_VAR_admin_aad_group_name="HavenAksAdmins"
terraform -chdir=./infrastructure init
terraform -chdir=./infrastructure plan --out=./planfile-infrastructure
terraform -chdir=./infrastructure apply ./planfile-infrastructure
```

### Tools

Tools augment the cluster with packages that make cluster VNG-compliant.

#### Get credentials & authenticate yourself

Before rolling out tools, a kubeconfig is needed. With the AAD integration, initial authentication is required.

```bash
az aks get-credentials --resource-group aks-haven --name aks -f ./kubeconfig
export KUBECONFIG=./kubeconfig
kubectl get po
```

#### Set kubeconfig variable

```bash
export TF_VAR_kubeconfig="$(pwd)/kubeconfig"
terraform -chdir=./tools init
terraform -chdir=./tools plan --out=./planfile-tools
terraform -chdir=./tools apply ./planfile-tools
```

## Run Haven Compliancy Checker

When ready ensure your new cluster is Haven Compliant by using the [Haven Compliancy Checker](../../../../haven/cli/README.md).

```bash
export KUBECONFIG=./kubeconfig
haven check --cncf=false
Haven v6.0.0 - Copyright © VNG Realisatie 2019-2023 - Licensed under the EUPL v1.2.

[I] Kubernetes connection using KUBECONFIG: ./kubeconfig.
[W] SSH: Not configured! Skipping compliancy checks requiring SSH access.
[W] CNCF: Opted out! Skipping external CNCF compliancy check.
[I] CIS: Not opted in. Skipping external CIS suggested check.
[I] Running checks...
[I] Latest stable Kubernetes release: v1.20.2.
[W] Results: 14 out of 15 checks passed, 1 checks skipped, 0 checks unknown. This COULD be a Haven Compliant cluster.

[I] Compliancy checks results:

+----------------+--------------------------------------------------------------------+---------+
|    CATEGORY    |                                NAME                                | PASSED  |
+----------------+--------------------------------------------------------------------+---------+
| Fundamental    | Self test: does HCC have cluster-admin                             | YES     |
| Infrastructure | Multiple availability zones in use                                 | YES     |
| Infrastructure | Running at least 3 master nodes                                    | YES     |
| Infrastructure | Running at least 3 worker nodes                                    | YES     |
| Infrastructure | Nodes have SELinux, Grsecurity, AppArmor or LKRG enabled           | YES     |
| Infrastructure | Private networking topology                                        | YES     |
| Cluster        | Kubernetes version is latest stable or max 2 minor versions behind | YES     |
| Cluster        | Role Based Access Control is enabled                               | YES     |
| Cluster        | Basic auth is disabled                                             | YES     |
| Cluster        | ReadWriteMany persistent volumes support                           | YES     |
| Cluster        | LoadBalancer service type support                                  | YES     |
| External       | CNCF Kubernetes Conformance                                        | SKIPPED |
| Deployment     | Automated HTTPS certificate provisioning                           | YES     |
| Deployment     | Log aggregation is running                                         | YES     |
| Deployment     | Metrics-server is running                                          | YES     |
+----------------+--------------------------------------------------------------------+---------+

[I] Suggested checks results:

+------------+-----------------------------------+---------+
|  CATEGORY  |               NAME                | PASSED  |
+------------+-----------------------------------+---------+
| External   | CIS Kubernetes Security Benchmark | SKIPPED |
+------------+-----------------------------------+---------+
```

## Cluster lifecycle management

To upgrade a cluster, modify cluster version in `variables.tf` file and execute `terraform apply` command.
Nodes are being updated automatically and the Kured add-on takes care of rebooting nodes to implement updates. Additional parameters can be set up in the module variables.


## License
Copyright © VNG Realisatie 2019-2023
Licensed under the EUPL

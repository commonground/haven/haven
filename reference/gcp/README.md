# Reference Implementation: Haven on Google Kubernetes Engine (GKE)

Builds a VPC with a GKE cluster and a Filestore instance in a GCP project, which can be used to
validate compliance and serve as a starting point for building GKE clusters.

Summary of resources created:

  - New VPC with necessary subnet and secondary ranges for GKE
  - Cloud NAT for internet access
  - Private regional GKE cluster with latest features
    - Enrolled in REGULAR update channel (current version as of time of writing: `1.20.10`)
    - `e2-standard-8` Shielded VM instances 100GB node disks with balanced PD configuration
    - Maintenance window enabled
    - Dataplane V2 (eBPF -based)
    - Workload Identity enabled
    - Compute Engine PD CSI driver add-on enabled
    - Anthos Policy Controller installed (managed Gatekeeper, version: `1.9.0`, optional but on by default)
      - Some policies installed for CIS controls (but in `dryrun` mode currently)
  - Basic Cloud Filestore instance for Kubernetes `ReadWriteMany` access
    - Includes deployment of NFS subdirectory provisioner

## Deploying

Create a `terraform.tfvars` file and minimally specify at least these variables:

```hcl
project_id  = "<YOUR-PROJECT-ID>"   # Project ID for existing GCP project
admin_email = "account@example.com" # The user or service account which you are using to deploy
```

If you are using this for compliance testing, please make sure `kubectl` is installed locally
in PATH.

### Deploying using a service account

First, login with `gcloud auth login` (you can skip this if you are already logged in).

### Bash

Create a service account for deploying the cluster:

```sh
export PROJECT=<your-project-id>
export SERVICE_ACCOUNT=gke-deployer
export MY_ACCOUNT=$(gcloud config get-value account)
export PROJECT_NUM=$(gcloud projects describe $PROJECT --format='value(projectNumber)')

gcloud services enable serviceusage.googleapis.com --project=$PROJECT
gcloud iam service-accounts create $SERVICE_ACCOUNT --project=$PROJECT

gcloud iam service-accounts add-iam-policy-binding "$SERVICE_ACCOUNT@$PROJECT.iam.gserviceaccount.com" \
  --member="user:$MY_ACCOUNT" \
  --role="roles/iam.serviceAccountTokenCreator" \
  --project=$PROJECT

# Project roles
for role in resourcemanager.projectIamAdmin compute.networkAdmin compute.securityAdmin \
  container.admin gkehub.editor file.editor serviceusage.serviceUsageAdmin iam.serviceAccountAdmin \
  iam.serviceAccountUser containeranalysis.admin binaryauthorization.attestorsEditor \
  binaryauthorization.policyAdmin
do
    gcloud projects add-iam-policy-binding $PROJECT --member="serviceAccount:$SERVICE_ACCOUNT@$PROJECT.iam.gserviceaccount.com" --role="roles/$role"
done

echo "Add to terraform.tfvars:"
echo "impersonate_service_account = \"$SERVICE_ACCOUNT@$PROJECT.iam.gserviceaccount.com\""
```

### PowerShell

Create a service account for deploying the cluster:

```powershell
$Env:PROJECT = "<your-project-id>"
$Env:SERVICE_ACCOUNT= "gke-deployer"
$Env:MY_ACCOUNT = gcloud config get-value account
$Env:PROJECT_NUM = gcloud projects describe $Env:PROJECT --format='value(projectNumber)'

gcloud services enable serviceusage.googleapis.com --project=$Env:PROJECT
gcloud iam service-accounts create $Env:SERVICE_ACCOUNT --project=$Env:PROJECT
gcloud iam service-accounts add-iam-policy-binding "${Env:SERVICE_ACCOUNT}@${Env:PROJECT}.iam.gserviceaccount.com" `
  --member="user:${Env:MY_ACCOUNT}" `
  --role="roles/iam.serviceAccountTokenCreator" `
  --project=$Env:PROJECT

foreach ($role in @("resourcemanager.projectIamAdmin", "compute.networkAdmin", "compute.securityAdmin", `
  "container.admin", "gkehub.editor", "file.editor", "serviceusage.serviceUsageAdmin", "iam.serviceAccountAdmin", `
  "iam.serviceAccountUser", "containeranalysis.admin", "binaryauthorization.attestorsEditor", `
  "binaryauthorization.policyAdmin")) {
    gcloud projects add-iam-policy-binding $Env:PROJECT `
      --member="serviceAccount:${Env:SERVICE_ACCOUNT}@${Env:PROJECT}.iam.gserviceaccount.com" `
      --role="roles/$role"
}

Write-Output @"
Add to terraform.tfvars:
impersonate_service_account = "${Env:SERVICE_ACCOUNT}@${Env:PROJECT}.iam.gserviceaccount.com"
"@

```

Now you can run `terraform init`, then `terraform plan` and `terraform apply` to
create the resources.

### Deploying using end-user credentials

After logging in with `gcloud auth application-default login`, run `terraform init`,
then `terraform plan` and finally `terraform apply` to create the resources. You will need to
have correct permissions to the project (see above for service account examples for a list
of IAM roles for least privileges, or if you have `Editor` or `Owner`, just add
`Container Analysis Admin` role to your end user account).

### Configuration parameters

<!-- BEGIN TFDOC -->
#### Variables

| name | description | type | required | default |
|---|---|:---: |:---:|:---:|
| admin_email | Username that you are authenticated under | <code title="">string</code> | ✓ |  |
| project_id | GCP project ID | <code title="">string</code> | ✓ |  |
| *cidr_range* | Node subnet CIDR | <code title="">string</code> |  | <code title="">192.168.164.0/24</code> |
| *cidr_range_controlplane* | GKE control plane range (/28) | <code title="">string</code> |  | <code title="">192.168.165.0/28</code> |
| *cidr_range_filestore* | Filestore CIDR | <code title="">string</code> |  | <code title="">192.168.165.16/29</code> |
| *cidr_range_ilb* | Internal HTTP(S) load balancing subnet range | <code title="">string</code> |  | <code title="">192.168.162.0/23</code> |
| *cidr_range_pods* | GKE pod CIDR | <code title="">string</code> |  | <code title="">192.168.128.0/20</code> |
| *cidr_range_services* | GKE services CIDR | <code title="">string</code> |  | <code title="">192.168.160.0/23</code> |
| *cluster_instance_type* | GKE cluster instance type | <code title="">string</code> |  | <code title="">e2-standard-8</code> |
| *cluster_name* | GKE cluster name | <code title="">string</code> |  | <code title="">haven-compliance-testing</code> |
| *compliance_testing_use* | Set to true if this cluster is being used for compliance testing | <code title="">bool</code> |  | <code title="">true</code> |
| *deploy_anthos_policy_controller* | Deployed managed Anthos Policy Controller with CIS policies | <code title="">bool</code> |  | <code title="">true</code> |
| *deploy_cis_policies* | Deploy sample CIS policies for Anthos Policy Controller | <code title="">bool</code> |  | <code title="">true</code> |
| *deploy_example_workload* | Deploy example Hello World nginx | <code title="">bool</code> |  | <code title="">true</code> |
| *enable_binary_authorization* | Enable binary authorization in cluster | <code title="">bool</code> |  | <code title="">true</code> |
| *impersonate_service_account* | Impersonate a service account | <code title="">string</code> |  | <code title=""></code> |
| *network* | VPC name | <code title="">string</code> |  | <code title="">haven-compliance</code> |
| *nfs_share_name* | NFS share name | <code title="">string</code> |  | <code title="">haven</code> |
| *region* | Region to deploy GKE into | <code title="">string</code> |  | <code title="">europe-west4</code> |

#### Outputs

| name | description | sensitive |
|---|---|:---:|
| cluster_name | None |  |
| cluster_region | None |  |
| credentials | None |  |
| network_name | None |  |
| nfs_host | None |  |
| nfs_share_name | None |  |
<!-- END TFDOC -->

## Haven check results

### Running

Instructions on how to run Haven are given in Terraform's output.

### Results

```
+----------------+--------------------------------------------------------------------+--------+
|    CATEGORY    |                                NAME                                | PASSED |
+----------------+--------------------------------------------------------------------+--------+
| Fundamental    | Self test: does HCC have cluster-admin                             | YES    |
| Infrastructure | Multiple availability zones in use                                 | YES    |
| Infrastructure | Running at least 3 master nodes                                    | YES    |
| Infrastructure | Running at least 3 worker nodes                                    | YES    |
| Infrastructure | Nodes have SELinux, Grsecurity, AppArmor or LKRG enabled           | YES    |
| Infrastructure | Private networking topology                                        | YES    |
| Cluster        | Kubernetes version is latest stable or max 2 minor versions behind | YES    |
| Cluster        | Role Based Access Control is enabled                               | YES    |
| Cluster        | Basic auth is disabled                                             | YES    |
| Cluster        | ReadWriteMany persistent volumes support                           | YES    |
| Cluster        | LoadBalancer service type support                                  | YES    |
| External       | CNCF Kubernetes Conformance                                        | YES    |
| Deployment     | Automated HTTPS certificate provisioning                           | YES    |
| Deployment     | Log aggregation is running                                         | YES    |
| Deployment     | Metrics-server is running                                          | YES    |
+----------------+--------------------------------------------------------------------+--------+

[I] Suggested checks results:

+------------+-----------------------------------+---------+
|  CATEGORY  |               NAME                | PASSED  |
+------------+-----------------------------------+---------+
| External   | CIS Kubernetes Security Benchmark | SKIPPED |
+------------+-----------------------------------+---------+
```

*Note:* kube-bench (CIS Kubernetes Security Benchmark) is not running the correct GKE
benchmark. See below for running manually.

## Sonobuoy check results

### Running manually

Running sonobuoy manually:

```sh
# sonobuoy run --mode=certified-conformance
```

### Results

```
         PLUGIN     STATUS   RESULT   COUNT               PROGRESS
            e2e   complete   passed       1   311/311 (0 failures)
   systemd-logs   complete   passed       3
```

## kube-bench (CIS) check results

### Running manually

Running kube-bench manually:

```sh
# kubectl apply -f https://raw.githubusercontent.com/aquasecurity/kube-bench/main/job-gke.yaml
```

### Results

```
== Summary managedservices ==
0 checks PASS
0 checks FAIL
37 checks WARN
0 checks INFO

== Summary total ==
12 checks PASS
0 checks FAIL
61 checks WARN
11 checks INFO
```

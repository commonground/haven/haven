# Google Kubernetes Engine

Deploys a GKE cluster.

<!-- BEGIN TFDOC -->
## Variables

| name | description | type | required | default |
|---|---|:---: |:---:|:---:|
| authenticator_security_group | The name of the RBAC security group for use with Google security groups in Kubernetes RBAC. Group name must be in format gke-security-groups@yourdomain.com | <code title="">string</code> | ✓ |  |
| cluster_name | Name of the Kubernetes cluster to be created | <code title="">string</code> | ✓ |  |
| cluster_resource_labels | The GCE resource labels (a map of key/value pairs) to be applied to the cluster | <code title="map&#40;string&#41;">map(string)</code> | ✓ |  |
| disk_size_gb | Size of the disk attached to each node, specified in GB. The smallest allowed disk size is 10GB. | <code title="">number</code> | ✓ |  |
| disk_type | Type of the disk attached to each node (e.g. 'pd-standard' or 'pd-ssd') | <code title="">string</code> | ✓ |  |
| gke_network | The VPC network to host the cluster in | <code title="">string</code> | ✓ |  |
| gke_subnet | The subnetwork to host the cluster in | <code title="">string</code> | ✓ |  |
| ip_range_controlplane | Control plane IP range | <code title="">string</code> | ✓ |  |
| ip_range_pods | The name of the secondary subnet ip range to use for pods | <code title="">string</code> | ✓ |  |
| ip_range_services | The name of the secondary subnet range to use for services | <code title="">string</code> | ✓ |  |
| machine_type | The name of a Google Compute Engine machine type | <code title="">string</code> | ✓ |  |
| maintenance_end_time | Time window specified for daily or recurring maintenance operations in RFC3339 format | <code title="">string</code> | ✓ |  |
| maintenance_recurrence | Frequency of the recurring maintenance window in RFC5545 format | <code title="">string</code> | ✓ |  |
| maintenance_start_time | Time window specified for daily or recurring maintenance operations in RFC3339 format | <code title="">string</code> | ✓ |  |
| max_nodes | Maximum number of nodes in the NodePool. Must be >= min_nodes | <code title="">number</code> | ✓ |  |
| max_pods_per_node | The maximum number of pods per node | <code title="">number</code> | ✓ |  |
| min_nodes | Minimum number of nodes in the NodePool. Must be >=0 and <= max_nodes. Should be used when autoscaling is true | <code title="">string</code> | ✓ |  |
| node_pool_name | The name of the node pool | <code title="">string</code> | ✓ |  |
| node_pools_tags | Map of lists containing node network tags by node-pool name | <code title="list&#40;string&#41;">list(string)</code> | ✓ |  |
| project_id | The Project ID to host the cluster in | <code title="">string</code> | ✓ |  |
| project_number | GCP project number | <code title="">number</code> | ✓ |  |
| region | The region to host the cluster in | <code title="">string</code> | ✓ |  |
| *enable_binary_authorization* | Enable binary authorization | <code title="">bool</code> |  | <code title="">false</code> |
| *enable_l4_ilb_subsetting* | Enable L4 ILB subsetting | <code title="">bool</code> |  | <code title="">false</code> |
| *service_account_roles* | Roles to grant the GKE node service account | <code title="list&#40;string&#41;">list(string)</code> |  | <code title="&#91;&#10;&#34;roles&#47;monitoring.viewer&#34;,&#10;&#34;roles&#47;monitoring.metricWriter&#34;,&#10;&#34;roles&#47;logging.logWriter&#34;,&#10;&#34;roles&#47;stackdriver.resourceMetadata.writer&#34;,&#10;&#93;">...</code> |
| *use_cloud_dns* | Use VPC-scoped cluster DNS | <code title="">bool</code> |  | <code title="">false</code> |
| *use_cmek* | Set up Customer Managed Encryption Keys | <code title="">bool</code> |  | <code title="">false</code> |

## Outputs

| name | description | sensitive |
|---|---|:---:|
| ca_certificate | GKE control plane CA certificate |  |
| cluster_id | None |  |
| cluster_name | None |  |
| cluster_network | None |  |
| cluster_region | None |  |
| kubernetes_endpoint | GKE control plane endpoint | ✓ |
| service_account | The default service account used for running nodes. |  |
<!-- END TFDOC -->

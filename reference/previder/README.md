---
title: "Previder Kubernetes Engine"
path: "/aan-de-slag/previder"
---

## Reference implementation: Haven on Previder Kubernetes Engine

You can easily deploy Kubernetes on Previder IaaS using [PKE](https://previder.nl/diensten/kubernetes-as-a-service).

**Trusted and local**

You can quickly and easily deploy your applications on our stable cloud platform, from our own high-end [data centers in the Netherlands](https://previder.nl/diensten/datacenter). In addition, our specialists provide support for this themselves.

**Redundancy**

With Kubernetes as a Service, it is now possible to deploy clusters simultaneously across Previder's data center locations.

### Prerequisites

* You must have a verified account on https://portal.previder.nl
* The following tools are required
  * OpenTofu (https://opentofu.org/docs/intro/install/)
  * kubectl
* NFS storage must be available in your network. Preferably using Previder STaaS (https://portal.previder.nl/#/storage/staas)
* In your network the following services should be present:
  * Internet access (Previder Managed Firewall / Custom firewall)
  * DHCP
  * DNS
  
### Installation

#### From scratch
1. Previder provides step-by-step documentation for installing a Kubernetes cluster (only in dutch) in our [Manual](https://github.com/previder/kubernetes-examples/blob/f92ca670522be04083e0a53180554c8cb344ad83/docs/installatiehandleiding-kubernetes-cluster.pdf).

#### Existing Previder IaaS environment
1. Login to https://portal.previder.com
2. In the navigation menu choose for Kubernetes and select Clusters
3. Press the button "New cluster"
  * Step 1: Choose a cluster type (eg. Express)
  * Step 2: Make sure that "High availability" is enabled and "Advanced settings" is disabled
  * Step 3: Make sure "Advanced settings" is disabled and choose Worker nodes size: Medium
  * Step 4:
    * Provide the name of the cluster
    * Disable automatic updates and select version 1.30.x. (or any other [Certified Kubernetes](https://github.com/cncf/k8s-conformance) version)
  * Step 5: 
    * Select "Calico" in the "Preinstall CNI in cluster" section
    * Check "I have created a network in ..." and select a network
    * Enter the address you are going to use to connect to the kubernetes cluster in the "VIP" section.
  * Press the button "Create"
  * Navigate to your cluster and download the kubeconfig in the Endpoints tab.

### Configuration
  1. Use OpenTofu to install the required software in your cluster. In order to do this you will need the kubeconfig you downloaded previously.
     * Navigate to the [terraform/configuration](./terraform/configuration) folder.
     * Modify variables.tf
       * Specify path to the kubeconfig (default: "~/.kube/config") in the `kube_config` variable block
       * Specify NFS server and NFS path in the `storage` variable block
     * Run the following command:
       ```shell
       tofu init
       tofu plan
       ```
       Verify the output. If everything is correct then use the following command to install the software.
       ```shell
       tofu apply
       ```
  2. Run Haven Compliancy checker. Instructions can be found here: [here](https://haven.commonground.nl/techniek/compliancy-checker).

### Result
```shell
[I] Results: 16 out of 16 checks passed, 0 checks skipped, 0 checks unknown. This is a Haven Compliant cluster.

[I] Compliancy checks results:

+----------------+--------------------------------------------------------------------------+--------+
|    CATEGORY    |                                   NAME                                   | PASSED |
+----------------+--------------------------------------------------------------------------+--------+
| Fundamental    | Self test: HCC version is latest major or within 3 months upgrade window | YES    |
| Fundamental    | Self test: does HCC have cluster-admin                                   | YES    |
| Infrastructure | Multiple availability zones in use                                       | YES    |
| Infrastructure | Running at least 3 master nodes                                          | YES    |
| Infrastructure | Running at least 3 worker nodes                                          | YES    |
| Infrastructure | Nodes have SELinux, Grsecurity, AppArmor, LKRG, Talos or Flatcar enabled | YES    |
| Infrastructure | Private networking topology                                              | YES    |
| Cluster        | Kubernetes version is latest stable or max 3 minor versions behind       | YES    |
| Cluster        | Role Based Access Control is enabled                                     | YES    |
| Cluster        | Basic auth is disabled                                                   | YES    |
| Cluster        | ReadWriteMany persistent volumes support                                 | YES    |
| External       | CNCF Kubernetes Conformance                                              | YES    |
| Deployment     | Automated HTTPS certificate provisioning                                 | YES    |
| Deployment     | Log aggregation is running                                               | YES    |
| Deployment     | Metrics-server is running                                                | YES    |
| Validation     | SHA has been validated                                                   | YES    |
+----------------+--------------------------------------------------------------------------+--------+

[I] Suggested checks results:

+----------+-----------------------------------+---------+
| CATEGORY |               NAME                | PASSED  |
+----------+-----------------------------------+---------+
| External | CIS Kubernetes Security Benchmark | SKIPPED |
| External | Kubescape                         | SKIPPED |
+----------+-----------------------------------+---------+

```
#### Infrastructuur as code

It is possible to deploy a kubernetes cluster using OpenTofu. More information can be found [here](./terraform/infrastructure)

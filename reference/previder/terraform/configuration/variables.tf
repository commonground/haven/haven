# Global
variable "kube_config" {
  description = "Kubeconfig settings"
  default = {
    path    = "~/.kube/config"
    context = ""
  }
}

variable "metrics_server" {
  description = "Metrics server"
  default = {
    enabled             = true
    name                = "metrics-server"
    repository          = "https://kubernetes-sigs.github.io/metrics-server"
    chart               = "metrics-server"
    chart_version       = "3.12.1"
    create_namespace    = true
    namespace           = "metrics-server"
    deployment_replicas = 1
    extra_args = ["--kubelet-insecure-tls"]
  }
}

variable "cert_manager" {
  description = "Cert Manager"
  default = {
    enabled                    = true
    name                       = "cert-manager"
    repository                 = "https://charts.jetstack.io"
    chart                      = "cert-manager"
    chart_version              = "v1.14.4"
    create_namespace           = true
    namespace                  = "cert-manager"
    deployment_replicas        = 1
    install_crds               = true
    recursive_nameservers      = "80.65.96.50:53\\,62.165.127.222:53"
    recursive_nameservers_only = true
    pdns_enabled               = true
    pdns_repository            = "https://zachomedia.github.io/cert-manager-webhook-pdns"
    pdns_chart                 = "cert-manager-webhook-pdns"
    pdns_chart_version         = "v3.1.1"
  }
}

variable "storage" {
  description = "Previder STaaS settings"
  default = {
    enabled           = true
    name              = "nfs-subdir-external-provisioner"
    repository        = "https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner/"
    chart             = "nfs-subdir-external-provisioner"
    chart_version     = "v4.0.18"
    create_namespace  = true
    namespace         = "previder-staas"
    nfs_server        = null
    nfs_path          = null
    nfs_storage_class = "previder-staas"
  }
}

variable "logging" {
  description = "FluentD settings"
  default = {
    enabled          = true
    name             = "fluent-bit"
    repository       = "https://fluent.github.io/helm-charts"
    chart            = "fluent-bit"
    chart_version    = "0.47.2"
    create_namespace = true
    namespace        = "fluent-bit"
  }
}

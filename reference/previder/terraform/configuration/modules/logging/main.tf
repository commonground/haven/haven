resource "helm_release" "fluent-bit" {
  name             = var.logging_name
  repository       = var.logging_repository
  chart            = var.logging_chart
  version          = var.logging_chart_version
  namespace        = var.logging_namespace
  create_namespace = var.logging_create_namespace
}
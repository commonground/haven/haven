variable "cert_manager_enabled" {
  description = "Enable Cert Manager"
  type        = bool
}

variable "cert_manager_name" {
  description = "Name of the instance"
  type        = string
}

variable "cert_manager_repository" {
  description = "Repository URL"
  type        = string
}

variable "cert_manager_chart" {
  description = "Helm Chart"
  type        = string
}

variable "cert_manager_chart_version" {
  description = "Helm Chart version"
  type        = string
}

variable "cert_manager_create_namespace" {
  description = "Create namespace"
  type        = bool
}

variable "cert_manager_namespace" {
  description = "Namespace name"
  type        = string
}

variable "cert_manager_deployment_replicas" {
  description = "Number of replicas in the deployment"
  type        = number
}

variable "cert_manager_install_crds" {
  description = "Install crds for Cert Manager"
  type        = bool
}

variable "cert_manager_recursive_nameservers" {
  description = "Specific nameservers for PDNS checks"
  type        = string
}

variable "cert_manager_recursive_nameservers_only" {
  description = "Use the specified nameservers only"
  type        = bool
}

variable "cert_manager_pdns_enabled" {
  description = "PDNS webhook enabled for Previder Portal"
  type        = bool
}

variable "cert_manager_pdns_repository" {
  description = "PDNS webhook Repository URL"
  type        = string
}

variable "cert_manager_pdns_chart" {
  description = "PDNS webhook Helm Chart"
  type        = string
}

variable "cert_manager_pdns_chart_version" {
  description = "PDNS webhook Helm Chart version"
  type        = string
}


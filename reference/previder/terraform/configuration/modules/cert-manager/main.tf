resource "helm_release" "cert-manager" {
  count            = var.cert_manager_enabled ? 1 : 0
  name             = var.cert_manager_name
  repository       = var.cert_manager_repository
  chart            = var.cert_manager_chart
  version          = var.cert_manager_chart_version
  namespace        = var.cert_manager_namespace
  create_namespace = var.cert_manager_create_namespace

  set {
    name  = "replicaCount"
    value = var.cert_manager_deployment_replicas
  }

  set {
    name  = "installCRDs"
    value = var.cert_manager_install_crds
  }

  set {
    name  = "dns01RecursiveNameservers"
    value = var.cert_manager_recursive_nameservers
  }

  set {
    name  = "dns01RecursiveNameserversOnly"
    value = var.cert_manager_recursive_nameservers_only
  }
}

resource "helm_release" "cert-manager-webhook-pdns" {
  count = var.cert_manager_pdns_enabled ? 1 : 0
  depends_on = [
    helm_release.cert-manager
  ]
  name = join("-", [var.cert_manager_name, "webhook-pdns"])
  repository       = var.cert_manager_pdns_repository
  chart            = var.cert_manager_pdns_chart
  version          = var.cert_manager_pdns_chart_version
  namespace        = var.cert_manager_namespace
  create_namespace = var.cert_manager_create_namespace

}
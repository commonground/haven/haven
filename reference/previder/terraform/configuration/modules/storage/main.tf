resource "helm_release" "nfs-subdir-external-provisioner" {
  count            = var.storage_enabled && var.storage_nfs_server != null && var.storage_nfs_path != null ? 1 : 0
  name             = var.storage_name
  repository       = var.storage_repository
  chart            = var.storage_chart
  version          = var.storage_chart_version
  namespace        = var.storage_namespace
  create_namespace = var.storage_create_namespace

  set {
    name  = "nfs.server"
    value = var.storage_nfs_server
  }

  set {
    name  = "nfs.path"
    value = var.storage_nfs_path
  }

  set {
    name  = "storageClass.name"
    value = var.storage_nfs_storage_class
  }
}
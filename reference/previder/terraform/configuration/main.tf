module "metrics_server" {
  source                             = "./modules/metrics-server"
  metrics_server_enabled             = var.metrics_server.enabled
  metrics_server_name                = var.metrics_server.name
  metrics_server_repository          = var.metrics_server.repository
  metrics_server_chart               = var.metrics_server.chart
  metrics_server_chart_version       = var.metrics_server.chart_version
  metrics_server_create_namespace    = var.metrics_server.create_namespace
  metrics_server_namespace           = var.metrics_server.namespace
  metrics_server_extra_args          = var.metrics_server.extra_args
  metrics_server_deployment_replicas = var.metrics_server.deployment_replicas
}

module "cert-manager" {
  source                                  = "./modules/cert-manager"
  cert_manager_enabled                    = var.cert_manager.enabled
  cert_manager_name                       = var.cert_manager.name
  cert_manager_repository                 = var.cert_manager.repository
  cert_manager_chart                      = var.cert_manager.chart
  cert_manager_chart_version              = var.cert_manager.chart_version
  cert_manager_create_namespace           = var.cert_manager.create_namespace
  cert_manager_namespace                  = var.cert_manager.namespace
  cert_manager_deployment_replicas        = var.cert_manager.deployment_replicas
  cert_manager_install_crds               = var.cert_manager.install_crds
  cert_manager_recursive_nameservers      = var.cert_manager.recursive_nameservers
  cert_manager_recursive_nameservers_only = var.cert_manager.recursive_nameservers_only
  cert_manager_pdns_enabled               = var.cert_manager.pdns_enabled
  cert_manager_pdns_repository            = var.cert_manager.pdns_repository
  cert_manager_pdns_chart                 = var.cert_manager.pdns_chart
  cert_manager_pdns_chart_version         = var.cert_manager.pdns_chart_version
}

module "storage" {
  source                    = "./modules/storage"
  storage_enabled           = var.storage.enabled
  storage_name              = var.storage.name
  storage_repository        = var.storage.repository
  storage_chart             = var.storage.chart
  storage_chart_version     = var.storage.chart_version
  storage_create_namespace  = var.storage.create_namespace
  storage_namespace         = var.storage.namespace
  storage_nfs_server        = var.storage.nfs_server
  storage_nfs_path          = var.storage.nfs_path
  storage_nfs_storage_class = var.storage.nfs_storage_class

}

module "logging" {
  source                   = "./modules/logging"
  logging_enabled          = var.logging.enabled
  logging_name             = var.logging.name
  logging_repository       = var.logging.repository
  logging_chart            = var.logging.chart
  logging_chart_version    = var.logging.chart_version
  logging_create_namespace = var.logging.create_namespace
  logging_namespace        = var.logging.namespace
}

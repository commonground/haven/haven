variable "kubernetes_cluster" {
  description = "Kubernetes cluster"
  default = {
    enabled                      = false
    name                         = "haven-compliant-cluster"
    cni                          = "calico"
    network                      = "haven-cluster-network"
    vips = ["192.168.1.220"]
    endpoints                    = null
    version                      = "1.30.2"
    auto_update                  = false
    auto_scale_enabled           = false
    minimal_nodes                = 1
    maximal_nodes                = 3
    control_plane_cpu_cores      = 2
    control_plane_memory_gb      = 2
    control_plane_storage_gb     = 25
    node_cpu_cores               = 4
    node_memory_gb               = 8
    node_storage_gb              = 50
    compute_cluster              = "express"
    high_available_control_plane = true
  }
}

variable "virtual_network" {
  description = "Virtualnetwork"
  default = {
    enabled = false
    name    = "haven-cluster-network"
    type    = "VLAN"
    group   = null
  }
}

variable "previder_config" {
  description = "Previder Provider Configuration"
  default = {
    token    = "<insert-token-here>"
    url      = null
    customer = null
  }
}
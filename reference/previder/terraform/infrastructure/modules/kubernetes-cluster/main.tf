resource "previder_kubernetes_cluster" "previder" {
  count                        = var.kubernetes_cluster_enabled ? 1 : 0
  name                         = var.kubernetes_cluster_name
  cni                          = var.kubernetes_cluster_cni
  network                      = var.kubernetes_cluster_network
  vips                         = var.kubernetes_cluster_vips
  endpoints                    = var.kubernetes_cluster_endpoints
  version                      = var.kubernetes_cluster_version
  auto_update                  = var.kubernetes_cluster_auto_update
  auto_scale_enabled           = var.kubernetes_cluster_auto_scale_enabled
  minimal_nodes                = var.kubernetes_cluster_minimal_nodes
  control_plane_cpu_cores      = var.kubernetes_cluster_control_plane_cpu_cores
  control_plane_memory_gb      = var.kubernetes_cluster_control_plane_memory_gb
  control_plane_storage_gb     = var.kubernetes_cluster_control_plane_storage_gb
  node_cpu_cores               = var.kubernetes_cluster_node_cpu_cores
  node_memory_gb               = var.kubernetes_cluster_node_memory_gb
  node_storage_gb              = var.kubernetes_cluster_node_storage_gb
  compute_cluster              = var.kubernetes_cluster_compute_cluster
  high_available_control_plane = var.kubernetes_cluster_high_available_control_plane
}
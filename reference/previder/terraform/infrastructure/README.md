## Infrastructure as code
To deploy a Virtual Network and a Kubernetes Cluster in the Previder Portal, Previder developed their own [Terraform/OpenTofu provider](https://github.com/previder/terraform-provider-previder). This Provider is registered at both the Terraform and OpenTofu registries for easy use.

### Authentication
To authenticate to the Previder Portal, we need a token. There are 2 kinds op tokens available in the Previder Portal

#### User token
A user token enables the user to automate actions as their own user account. This token will have the same privileges as the user and is only advised as a token to test with.

To create a user token, navigate to the [User settings](https://portal.previder.com/#/user/current/properties) via the Top menu, and then clicking the "User settings" item.

Open the "Token" tab and click the "Create token" button. Enter a descriptive name and create the token. After creation, copy the token as this will only be displayed once.

#### Application token
An application token is a more secure token and is recommended for long-term applications. The authorization can be more fine-grained because the role of the application can be configured as-needed.

1. Create a [role](https://portal.previder.nl/#/user/role) with a descriptive name and set up the permissions required for the to-set up resources
1. Create an [application](https://portal.previder.nl/#/application) and while creating choose the role as created in step 1 
1. After creating the application, select it in list and open the "Tokens" tab
1. Create a new token, enter a descriptive name and optionally choose an expiry date
1. Copy the created token as this is only displayed once

### Infrastructure
If you did not create a network yet, create your own network via the Previder Portal, or enable the `virtual_network_enabled` variable. Note: as described in the [main README](../../README.md), you are responsible for the internet connectivity in the network.

Previder STaaS is not yet available as Infrastructure as Code, so a NFS volume needs to be created in the Previder Portal manually for now.

In the file [variables.tf](./variables.tf), replace the variables for a cluster as required.

Run the following commands:
```shell
tofu init
tofu plan
```
Verify the output. If everything is correct then use the following command to create the infrastructure.
```shell
tofu apply
```

# Kops on Openstack.

Haven Reference Implementation on OpenStack is based on Kops and requires OpenStack with Swift and LBaaSv2 (preferably Octavia v2.12+).

OpenStack allows for a lot of customization, everything works out best with providers that mostly stick to standard OpenStack.

#### Building the container
Prereq: place the [Haven CLI](../../../haven/cli) binary next to the Dockerfile, because the docker context requires this to COPY it in.

Now run: `docker build -t registry.gitlab.com/commonground/haven/haven/reference_openstack .` to build the container.

## Running the container
Everything is bundled into a Docker container which will be pulled automatically by the `haven_reference_openstack` wrapper.

Copy or symlink [./haven\_reference\_openstack.sh](./haven_reference_openstack.sh) to /usr/local/bin/haven\_reference\_openstack. Running the container will pull the docker image if it does not exist yet.

## Creating a cluster

As of Kops 1.26.x you have to manually create the OpenStack object store container first with name `kops-<CLUSTER_NAME>`.

Run `haven_reference_openstack <CLUSTER_NAME>` and from there run `create` to deploy your new Haven cluster. This will guide you through the nescessary steps to get started with a new cluster.

## Updating a cluster

First ensure you have a backup of everything you can backup. Best practice would be to try all your changes first on an identical Haven cluster, before you apply changes to a production cluster.

You can easily make changes to your cluster, for example changing the worker node flavor, adding bastion security group ip ranges or upgrading Kubernetes.
Simply try `kops edit cluster` or `kops edit ig nodes`.

When you are done editing run `kops update cluster` and confirm changes with `kops update cluster --yes`.
Sometimes you get a message hinting your cluster needs a rolling update, which will replace nodes one by one with newly configured nodes. Run `kops rolling-update cluster` and confirm changes with `kops rolling-update cluster --yes`.

## Upgrading a cluster

First ensure you have a backup of everything you can backup. Best practice would be to try all your changes first on an identical Haven cluster, before you apply changes to a production cluster.

Running a newer version of the container will automatically detect an available cluster upgrade. You can also manually run upgrades with: `upgrade`.

## Destroying a cluster

From the container run:

`destroy`

## Cluster management

From the container these tools are available to you:

`haven check` Runs the Haven Compliancy Checker\
`status` Shows an overview of cluster and pod health\
`create` Creates a new cluster\
`upgrade` Upgrades an existing cluster\
`destroy` Destroys an existing cluster\
`pool` Creates a new node pool\
`ssh bastion` SSH into the bastion host\
`ssh <node ip>` SSH into a node through the bastion host\
`openstack server list` Lists all servers including cluster nodes\
`openstack loadbalancer list` Lists all Octavia loadbalancers\
`neutron lbaas-loadbalancer-list` Lists all LBaaSv2 loadbalancers (legacy)\
`kops` Manages cluster\
`kubectl` Manages kubernetes\
`helm` Manages deployments\

Haven ships with bash completion enabled for kops, kubectl, helm and openstack.

For more details try `help`.

### Shared storage

Cinder block storage PersistentVolumes are enabled by default with Haven OpenStack Reference Implementation clusters and provide ReadWriteOnce volumes.

Example: [etc/example-pvc.yaml](etc/example-pvc.yaml).

### Node pools

With kops it's very easy to manage the cluster's instance groups (e.g. `kops edit ig nodes`) and create additional node pools.

A typical usecase could be creating a dedicated monitoring node pool running only critical deployments like ElasticSearch and Alerting. Effectively this means your regular application deployments can never take down the monitoring nodes which is critical when troubleshooting issues. Having a dedicated pool you can use a combination of affinity and tolerations to make this work.

To make it easier to create a new node pool having all Haven specifics in the spec you can use Haven's helper like this: `pool create <name>`. The script will duplicate your default nodes instance group and allow you to edit the specifics before actually creating it.

Please refer to the official kops documentation for more in depth information about Instance Groups: [https://github.com/kubernetes/kops/blob/master/docs/instance_groups.md](https://github.com/kubernetes/kops/blob/master/docs/instance_groups.md).

### Secrets

#### Usage

Upon cluster creation Haven asks you for a list of email addresses the master key will be gpg encrypted with. A master key will be generated for you. The master key will transparantly be used with ansible-vault to encrypt / decrypt secret files like your SSH private key and password variables inside yaml templates. Upon exiting Haven the master key is stored gpg encrypted in your state repository.

#### Re-encrypting the master key with a changed list of admins

From inside haven:

1. Edit $STATEPATH/master.key.admins to fit your needs.
2. `rm $STATEPATH/master.key.gpg`
3. `exit`

Haven will recreate master.key.gpg encrypted with the new secrets.admins public keys.

#### Updating an encrypted file

If you want to update openrc.sh for example, which is encrypted in your state repository, simple replace it with the new unencrypted version. When you run Haven it will detect the unencrypted file, use it and encrypt it for you.

#### Updating the master key

For example when someone in your team leaves you should roll all the keys. Problem is: rolling the master key is not good enough. Typically Kubernetes clusters are full of secrets all over the place, including some which cannot be rolled (like the master private certificate).

Therefore there is currently no way to roll the master key. To properly renew keys when you must, there is only one possibility: create a new cluster and migrate your workload.

## Q/A

Q: Where can I find more info?\
A: Everything including sourcecode can be found at https://gitlab.com/commonground/haven/haven. Kanban board direct link: https://gitlab.com/commonground/haven/haven/boards/963655.

Q: What should I expect from a configured cluster?\
A: Upon deployment you'll have a Kubernetes cluster on OpenStack which should be Haven Compliant. See: https://haven.commonground.nl/techniek.

Q: Can I run multiple clusters in a single OpenStack account/project?\
A: Yes, no specifics required.

Q: Where are all the moving parts stored on my host (e.g. laptop)?\
A: - Haven itself is stored where you've cloned the repository, but you can deal with just haven_ri_openstack.sh which pulls a docker image.
   - Cluster state is stored inside ~/.haven/state/<CLUSTER_NAME>

Q: What is the flow, what happens exactly when I open and close Haven?\
A: - When a Haven session is running the environments' state directory will be used as a volume inside the Haven docker container.

## License
Copyright © VNG Realisatie 2019-2023
Licensed under the EUPL

import { Stack } from "aws-cdk-lib";
import { ICluster } from "aws-cdk-lib/aws-eks";
import { Chart } from "cdk8s";
import { Namespace, ConfigMap } from "cdk8s-plus-21";
import { IConstruct } from "constructs";

export interface LoggingChartProps {
  cluster: ICluster;
}

/**
 * Install logging.
 *
 * @see https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/Container-Insights-setup-logs.html
 */
export class LoggingChart extends Chart {
  constructor(scope: IConstruct, id: string, props: LoggingChartProps) {
    super(scope, id, {
      namespace: "amazon-cloudwatch",
    });

    new Namespace(this, "cloudwatchNamespace", {
      metadata: {
        name: "amazon-cloudwatch",
        labels: {
          name: "amazon-cloudwatch",
        },
      },
    });

    const configMap = new ConfigMap(this, "clusterInfo", {
      metadata: {
        name: "cluster-info",
      },
    });

    configMap.addData("cluster.name", props.cluster.clusterName);
    configMap.addData("logs.region", Stack.of(props.cluster).region);
  }
}

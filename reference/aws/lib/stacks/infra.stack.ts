import { Stack, StackProps } from "aws-cdk-lib";
import {
  FlowLog,
  FlowLogDestination,
  FlowLogResourceType,
  Vpc,
} from "aws-cdk-lib/aws-ec2";
import { Role, ServicePrincipal } from "aws-cdk-lib/aws-iam";
import { IKey, Key } from "aws-cdk-lib/aws-kms";
import { LogGroup } from "aws-cdk-lib/aws-logs";
import { Construct } from "constructs";

/**
 * Stack containing the VPC and EFS volume
 */
export class InfraStack extends Stack {
  readonly vpc: Vpc;
  readonly envelopeEncryptionKey: IKey;

  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, {
      description: "Underlying infrastructure for the EKS cluster.",
      ...props,
    });

    this.vpc = new Vpc(this, "HavenVpc"); // specify manually, smaller subnets IP-range-wise
    // Separate Subnets for ENIs so we don't exhaust IPs

    // Create a flow log for the VPC
    const logGroup = new LogGroup(this, "FlowLogGroup");

    const role = new Role(this, "FlowLogAccessRole", {
      assumedBy: new ServicePrincipal("vpc-flow-logs.amazonaws.com"),
    });

    new FlowLog(this, "FlowLog", {
      resourceType: FlowLogResourceType.fromVpc(this.vpc),
      destination: FlowLogDestination.toCloudWatchLogs(logGroup, role),
    });

    this.envelopeEncryptionKey = new Key(this, "EksEnvelopeEncryptionKey", {
      enableKeyRotation: true,
    });
  }
}

import { Stack, StackProps } from "aws-cdk-lib";
import { IVpc } from "aws-cdk-lib/aws-ec2";
import {
  AlbControllerVersion,
  Cluster,
  ClusterLoggingTypes,
  EndpointAccess,
  KubernetesVersion,
} from "aws-cdk-lib/aws-eks";
import { IKey } from "aws-cdk-lib/aws-kms";
import { NagSuppressions } from "cdk-nag";
import { App } from "cdk8s";
import { Construct } from "constructs";

import { EfsVolumeChart } from "../charts/efs-volume.chart";
import { EfsForEks } from "../constructs/efs-eks";
import { UbuntuEksNodeGroup } from "../constructs/ubuntu-eks.node-group";

export type EksApiAccessType = "public" | "private" | "public_and_private";
export type Cidr = `${number}.${number}.${number}.${number}/${number}`;
export type Ami = `ami-${string}`;

export interface EksStackProps extends StackProps {
  vpc: IVpc;
  clusterNodeAmi: Ami;
  clusterName?: string;
  version: KubernetesVersion;
  eksApiType: EksApiAccessType;
  eksApiAccessCidrs: Cidr[];
  envelopeEncryptionKey: IKey;
}

/**
 * The stack that contains the EKS cluster
 */
export class ClusterStack extends Stack {
  readonly cluster: Cluster;
  readonly ubuntuNodeGroup: UbuntuEksNodeGroup;

  constructor(scope: Construct, id: string, props: EksStackProps) {
    super(scope, id, {
      description: "Contains the EKS Cluster",
      ...props,
    });

    const cdk8sApp = new App();

    let endpointAccess: EndpointAccess;

    switch (props.eksApiType) {
      case "public":
        endpointAccess = EndpointAccess.PUBLIC;
        break;
      case "public_and_private":
        endpointAccess = EndpointAccess.PUBLIC_AND_PRIVATE.onlyFrom(
          ...props.eksApiAccessCidrs
        );
        break;
      default:
      case "private":
        endpointAccess = EndpointAccess.PRIVATE;
        break;
    }

    /**
     * EKS Cluster
     */
    const cluster = new Cluster(this, "EksCluster", {
      version: props.version,
      clusterName: props.clusterName ?? `${id}-cluster`,
      vpc: props.vpc,
      endpointAccess,
      secretsEncryptionKey: props.envelopeEncryptionKey,

      albController: {
        version: AlbControllerVersion.V2_4_1,
      },

      defaultCapacity: 0, // set to zero so no default nodegroup is created
      clusterLogging: [
        ClusterLoggingTypes.API,
        ClusterLoggingTypes.AUDIT,
        ClusterLoggingTypes.AUTHENTICATOR,
        ClusterLoggingTypes.CONTROLLER_MANAGER,
        ClusterLoggingTypes.SCHEDULER,
      ],
    });

    this.cluster = cluster;

    /**
     * Ubuntu EKS Node Group
     */
    this.ubuntuNodeGroup = new UbuntuEksNodeGroup(this, "UbuntuEksNodegroup", {
      cluster,
      image: props.clusterNodeAmi,
      desiredSize: 3,
    });

    /**
     * EFS Volume
     */
    const efs = new EfsForEks(this, "HavenEfs", {
      vpc: props.vpc,
    });

    efs.grantSecurityGroupAccess(cluster.clusterSecurityGroup);

    cluster.addCdk8sChart(
      "EfsVolumeChart",
      new EfsVolumeChart(cdk8sApp, "EfsVolume", {
        efs,
      })
    );

    cluster.addHelmChart("AwsEfsCsiDriver", {
      chart: "aws-efs-csi-driver",
      repository: "https://kubernetes-sigs.github.io/aws-efs-csi-driver/",
      namespace: "kube-system",
    });

    const eksServiceAccount = cluster.addServiceAccount("EksServiceAccount", {
      labels: {
        "app.kubernetes.io/name": "aws-efs-csi-driver",
      },
    });

    efs.grantAccess(eksServiceAccount);
  }

  /**
   * Infrastructure checking suppressions.
   *
   * This adds overrides with justifications to the security scanner `cdk-nag`
   */
  private suppressInfrastructureWarnings(cluster: Cluster, id: string) {
    NagSuppressions.addResourceSuppressions(
      cluster,
      [
        {
          id: "AwsSolutions-EKS1",
          reason:
            "The cluster API is public so that a local kubectl may access it. " +
            "Access is restricted via AuthN and an IP filter. " +
            "Internal traffic remains inside the VPC.",
        },
      ],
      true
    );

    NagSuppressions.addResourceSuppressions(cluster.role, [
      {
        id: "AwsSolutions-IAM4",
        reason: "Using managed cluster policy",
        appliesTo: [
          "Policy::arn:<AWS::Partition>:iam::aws:policy/AmazonEKSClusterPolicy",
        ],
      },
    ]);

    NagSuppressions.addResourceSuppressionsByPath(
      this,
      `${id}/EksCluster/Resource/CreationRole`,
      [
        {
          id: "AwsSolutions-IAM5",
          reason: "Star permissions are needed for resource creation",
        },
      ],
      true
    );

    [
      `KubectlProvider/Handler/Resource`,
      `KubectlProvider/Provider/framework-onEvent`,
      `ClusterResourceProvider/Provider/framework-onTimeout`,
      `ClusterResourceProvider/IsCompleteHandler`,
      `ClusterResourceProvider/Provider/framework-isComplete`,
      `ClusterResourceProvider/OnEventHandler/Resource`,
      `ClusterResourceProvider/Provider/framework-onEvent`,
    ].forEach((item) =>
      NagSuppressions.addResourceSuppressionsByPath(
        this,
        `${id}/@aws-cdk--aws-eks.${item}`,
        [
          {
            id: "AwsSolutions-L1",
            reason:
              "This is a CDK internal construct, the runtime and layer versions will be updated in line with the CDK cadence.",
          },
        ],
        true
      )
    );

    [
      `KubectlProvider/Handler/ServiceRole`,
      `KubectlProvider/Provider/framework-onEvent/ServiceRole`,
      `ClusterResourceProvider/Provider/waiter-state-machine/Role`,
      `ClusterResourceProvider/Provider/framework-onEvent/ServiceRole`,
      `ClusterResourceProvider/Provider/framework-onTimeout/ServiceRole`,
      `ClusterResourceProvider/Provider/framework-isComplete/ServiceRole`,
      `ClusterResourceProvider/IsCompleteHandler/ServiceRole`,
      `ClusterResourceProvider/OnEventHandler/ServiceRole`,
    ].forEach((item) =>
      NagSuppressions.addResourceSuppressionsByPath(
        this,
        `${id}/@aws-cdk--aws-eks.${item}`,
        [
          {
            id: "AwsSolutions-IAM4",
            reason:
              "Managed policies are used to access CloudWatch, the VPC and the EC2 Container Registry",
            appliesTo: [
              "Policy::arn:<AWS::Partition>:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly",
              "Policy::arn:<AWS::Partition>:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole",
              "Policy::arn:<AWS::Partition>:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole",
            ],
          },
          {
            id: "AwsSolutions-IAM5",
            reason:
              "Wildcard permissions are scoped to the associated Lambda function.",
            appliesTo: [{ regex: /^Resource::<(.*).Arn>:\*$/.toString() }],
          },
        ],
        true
      )
    );
  }
}

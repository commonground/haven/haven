BINDIR       := $(CURDIR)/bin
INSTALL_PATH ?= /usr/local/bin
TARGETS      := darwin/amd64 darwin/arm64 linux/amd64 linux/arm64 windows/amd64
BINNAME      ?= haven

GOBIN         = $(shell go env GOBIN)
ifeq ($(GOBIN),)
GOBIN         = $(shell go env GOPATH)/bin
endif
GOX           = $(GOBIN)/gox

LDFLAGS := -w -s
LDFLAGS += -X main.version=${HAVEN_VERSION}

SRC := $(shell find . -type f -name '*.go' -print) go.mod go.sum

.PHONY: all
all: build

# ------------------------------------------------------------------------------
#  build
# ------------------------------------------------------------------------------

.PHONY: build
build: $(BINDIR)/$(BINNAME)

$(BINDIR)/$(BINNAME): $(SRC)
	GO111MODULE=on go build $(GOFLAGS) -trimpath -ldflags '$(LDFLAGS)' -o '$(BINDIR)'/$(BINNAME) ./cmd/cli

# ------------------------------------------------------------------------------
#  install
# ------------------------------------------------------------------------------

.PHONY: install
install: build
	@install "$(BINDIR)/$(BINNAME)" "$(INSTALL_PATH)/$(BINNAME)"

# ------------------------------------------------------------------------------
#  release
# ------------------------------------------------------------------------------

$(GOX):
	(cd /; GO111MODULE=on go install github.com/mitchellh/gox@v1.0.1)

.PHONY: build-cross
build-cross: LDFLAGS += -extldflags "-static"
build-cross: $(GOX)
	GOFLAGS="-trimpath" GO111MODULE=on CGO_ENABLED=0 $(GOX) -parallel=3 -output="_dist/{{.OS}}-{{.Arch}}/$(BINNAME)" -osarch='$(TARGETS)' $(GOFLAGS) -ldflags '$(LDFLAGS)' ./cmd/cli
	touch hashes.sha256
	for f in $$(ls _dist); do \
		binary_path="_dist/$$f/haven"; \
		if [[ $$f == *windows* ]]; then \
			binary_path="_dist/$$f/haven.exe"; \
		else \
			strip -v $$binary_path; \
		fi; \
		shasum --algorithm 256 $$binary_path >> hashes.sha256; \
		zip -r "_dist/haven-${HAVEN_VERSION}-$$f.zip" "_dist/$$f"; \
		rm -rf "_dist/$$f"; \
	done

release.json: hashes.sha256
	@json='{"date": "$(shell date --utc +"%Y-%m-%dT00:00:00Z")", "version": "$(HAVEN_VERSION)", "hashes": {}}'; \
	while read -r hash file; do \
		arch=$$(echo "$$file" | sed "s/_dist\/\(.*\)\/haven/\1/" | sed "s/-/\//"); \
		json=$$(echo "$$json" | jq --arg arch "$$arch" --arg hash "$$hash" '.hashes[$$arch] = $$hash'); \
	done < hashes.sha256; \
	echo "$$json" > release.json

.PHONY: add-release
add-release:
	@jq --slurpfile new_release release.json \
		'.releases = ($$new_release + .releases)' \
		../../docs/public/releases.json > tmp.$$$$ && mv tmp.$$$$ ../../docs/public/releases.json

.PHONY: info
info:
	@echo "Version:           ${HAVEN_VERSION}"

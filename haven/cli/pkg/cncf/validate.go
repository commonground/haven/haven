package cncf

import (
	"encoding/json"
	"fmt"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/handler"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
	"io"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"
)

// ConformancePlatformVersion starts the conformance check based on the platform and version of a `haven check`
func ConformancePlatformVersion(platform, version string) error {
	parsedVersion, err := getMajorAndMinor(version)
	if err != nil {
		return err
	}

	conformanceUrl, err := url.JoinPath(CONFORMANCEREPO, parsedVersion)
	if err != nil {
		return err
	}

	u, err := url.Parse(conformanceUrl)
	if err != nil {
		return err
	}

	// all certified kubernetes platforms are returned which match the parsedVersion (1.27)
	certifiedRepos, err := handler.Request(*u, nil, "")
	if err != nil {
		return err
	}

	if certifiedRepos.StatusCode != http.StatusOK {
		return fmt.Errorf("failed to fetch certified repos: %s", certifiedRepos.Status)
	}

	defer certifiedRepos.Body.Close()
	body, err := io.ReadAll(certifiedRepos.Body)
	if err != nil {
		return err
	}

	var data []repoData
	err = json.Unmarshal(body, &data)
	if err != nil {
		return err
	}

	// match the repo with the `haven check` platform
	repo, err := matchRepo(data, platform)
	if err != nil {
		return err
	}

	return checkConformanceInLog(repo)
}

// checkConformanceInLog checks the conformance in the log of a repository.
// The function fetches the content of the repository, reads the e2e.log file, and searches using a regular expression.
func checkConformanceInLog(repo *repoData) error {
	paresdUrl, err := url.Parse(repo.Url)
	if err != nil {
		return err
	}
	content, err := handler.Request(*paresdUrl, nil, "")
	if err != nil {
		return err
	}

	if content.StatusCode != http.StatusOK {
		return fmt.Errorf("failed to fetch content: %s", content.Status)
	}

	defer content.Body.Close()
	body, err := io.ReadAll(content.Body)
	if err != nil {
		return err
	}

	var data []repoData
	err = json.Unmarshal(body, &data)
	if err != nil {
		return err
	}

	var e2eLogFile string

	for _, file := range data {
		if file.Name == LOGFILE {
			fileUrl, err := url.Parse(file.DownloadUrl)
			if err != nil {
				return err
			}
			fileContent, err := handler.Request(*fileUrl, nil, "")
			if err != nil {
				return err
			}

			if content.StatusCode != http.StatusOK {
				return fmt.Errorf("failed to fetch content: %s", content.Status)
			}

			defer content.Body.Close()
			logFile, err := io.ReadAll(fileContent.Body)
			if err != nil {
				return err
			}

			e2eLogFile = string(logFile)
		}
	}

	// Define regular expression based on this:
	// Ran 368 of 7069 Specs in 6829.442 seconds
	// SUCCESS! -- 368 Passed | 0 Failed | 0 Pending | 6701 Skipped
	// these are lines from the e2e.log that are included in the repo
	pattern := `Ran (\d+) of \d+ Specs|(\d+) Passed \| (\d+) Failed`
	re := regexp.MustCompile(pattern)

	// Search for matches
	matches := re.FindAllStringSubmatch(e2eLogFile, -1)

	var specsRun, passed, failed int
	for _, match := range matches {
		if match[1] != "" {
			specsRun, err = strconv.Atoi(match[1])
			if err != nil {
				return err
			}
		} else if match[2] != "" && match[3] != "" {
			passed, err = strconv.Atoi(match[2])
			if err != nil {
				return err
			}
			failed, err = strconv.Atoi(match[3])
			if err != nil {
				return err
			}
		}
	}

	logging.Info(fmt.Sprintf("Parsed e2e.log from %s | passed: %d | failed %d | total: %d", repo.Url, passed, failed, specsRun))

	if failed != 0 {
		return fmt.Errorf("there are failures in the e2e.log please make sure you are using a certified CNCF kubernetes cluster")
	}

	return nil
}

// matchRepo checks the given list of repositories and matches the platform name with the repo name.
func matchRepo(repos []repoData, platform string) (*repoData, error) {
	platform = strings.ToLower(platform)
	// since our BestEffortPlatform tends to be quite verbose such as "Haven Cloud Platform"
	// the following strings are checked: "haven" "cloud" and "platform" from the example.
	splitPlatform := strings.Split(platform, " ")

	for _, p := range splitPlatform {
		for _, repo := range repos {
			// if the name matches the repo is returned
			if p == repo.Name {
				return &repo, nil
			}
		}
	}
	return nil, fmt.Errorf("could not match the running plaform with a known k8s conformance product")
}

// getMajorAndMinor extracts the major and minor version from the given version string using a Regex.
// Example outcome v1.27.3-ko3 -> v1.27 the major and minor are needed to search the conformance github repo
func getMajorAndMinor(version string) (string, error) {
	re := regexp.MustCompile(`(v\d+\.\d+)\.\d+.*`)
	matches := re.FindStringSubmatch(version)
	if len(matches) != 2 {
		return "", fmt.Errorf("invalid version string")
	}

	return matches[1], nil
}

package cncf

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestConformancePlatformVersion(t *testing.T) {

	t.Run("TestConformancePlatformVersion", func(t *testing.T) {
		version := "v1.26.1"
		platform := "Microsoft Azure AKS"
		err := ConformancePlatformVersion(platform, version)
		assert.Nil(t, err)
	})

	// issue 647
	t.Run("TestConformancePlatformVersionRKEVersion10", func(t *testing.T) {
		version := "v1.27.10+rke2r1"
		platform := "RKE"
		err := ConformancePlatformVersion(platform, version)
		assert.Nil(t, err)
	})

	// issue 647
	t.Run("TestConformancePlatformVersionRKEVersion11", func(t *testing.T) {
		version := "v1.27.11+rke2r1"
		platform := "RKE"
		err := ConformancePlatformVersion(platform, version)
		assert.Nil(t, err)
	})

	t.Run("TestConformancePlatformPrevider", func(t *testing.T) {
		version := "v1.30.3"
		platform := "Previder"
		err := ConformancePlatformVersion(platform, version)
		assert.Nil(t, err)
	})
}

package cncf

const (
	LOGFILE         = "e2e.log"
	CONFORMANCEREPO = "https://api.github.com/repos/cncf/k8s-conformance/contents/"
)

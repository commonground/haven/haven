package compliancy

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func TestDeploymentLogAggregation(t *testing.T) {
	t.Run("LoggersPositive", func(t *testing.T) {
		err := initWhitelist()
		assert.Nil(t, err)
		testClient := kubernetes.NewFakeClientset()
		ns := "test"

		config := Config{
			Kube: testClient,
		}

		for _, logger := range whitelist.Loggers {
			err = kubernetes.CreatePodForTest(logger.Name, ns, testClient)
			assert.Nil(t, err)

			result, err := deploymentLogAggregation(context.Background(), &config)
			assert.Nil(t, err)
			assert.Equal(t, result, ResultYes)

			err = testClient.CoreV1().Pods(ns).Delete(context.TODO(), logger.Name, *v1.NewDeleteOptions(0))
			assert.Nil(t, err)
		}
	})

	t.Run("LoggersNegative", func(t *testing.T) {
		err := initWhitelist()
		assert.Nil(t, err)
		testClient := kubernetes.NewFakeClientset()
		ns := "test"

		config := Config{
			Kube: testClient,
		}

		err = kubernetes.CreatePodForTest("notaloggernameatall", ns, testClient)
		assert.Nil(t, err)

		result, err := deploymentLogAggregation(context.Background(), &config)
		assert.Nil(t, err)
		assert.Equal(t, result, ResultNo)
	})

	t.Run("MetricsPositive", func(t *testing.T) {
		err := initWhitelist()
		assert.Nil(t, err)
		testClient := kubernetes.NewFakeClientset()
		ns := "test"

		config := Config{
			Kube: testClient,
		}

		for _, metric := range whitelist.Metrics {
			err = kubernetes.CreatePodForTest(metric.Name, ns, testClient)
			assert.Nil(t, err)

			result, err := deploymentMetricsServer(context.Background(), &config)
			assert.Nil(t, err)
			assert.Equal(t, result, ResultYes)

			err = testClient.CoreV1().Pods(ns).Delete(context.TODO(), metric.Name, *v1.NewDeleteOptions(0))
			assert.Nil(t, err)
		}
	})

	t.Run("MetricsNegative", func(t *testing.T) {
		err := initWhitelist()
		assert.Nil(t, err)
		testClient := kubernetes.NewFakeClientset()
		ns := "test"

		config := Config{
			Kube: testClient,
		}

		err = kubernetes.CreatePodForTest("notamtricservicatall", ns, testClient)
		assert.Nil(t, err)

		result, err := deploymentMetricsServer(context.Background(), &config)
		assert.Nil(t, err)
		assert.Equal(t, result, ResultNo)
	})

	t.Run("ProviderPodsPositive", func(t *testing.T) {
		err := initWhitelist()
		assert.Nil(t, err)
		testClient := kubernetes.NewFakeClientset()
		ns := "test"

		config := Config{
			Kube: testClient,
		}

		for _, pod := range whitelist.CertPods {
			err = kubernetes.CreatePodForTest(pod.Name, ns, testClient)
			assert.Nil(t, err)

			result, err := deploymentHttpsCerts(context.Background(), &config)
			assert.Nil(t, err)
			assert.Equal(t, result, ResultYes)

			err = testClient.CoreV1().Pods(ns).Delete(context.TODO(), pod.Name, *v1.NewDeleteOptions(0))
			assert.Nil(t, err)
		}
	})

	t.Run("ProviderPodsNegative", func(t *testing.T) {
		err := initWhitelist()
		assert.Nil(t, err)
		testClient := kubernetes.NewFakeClientset()
		ns := "test"

		config := Config{
			Kube: testClient,
		}

		err = kubernetes.CreatePodForTest("notaproviderpodatall", ns, testClient)
		assert.Nil(t, err)

		result, err := deploymentHttpsCerts(context.Background(), &config)
		assert.Nil(t, err)
		assert.Equal(t, result, ResultNo)
	})

	t.Run("CrdsPositive", func(t *testing.T) {
		err := initWhitelist()
		assert.Nil(t, err)
		testClient := kubernetes.NewFakeClientset()

		config := Config{
			Kube: testClient,
		}

		for _, crd := range whitelist.CertCrds {
			err = kubernetes.CreateCrdForTest(crd.Name, testClient)
			assert.Nil(t, err)

			result, err := deploymentHttpsCerts(context.Background(), &config)
			assert.Nil(t, err)
			assert.Equal(t, result, ResultYes)

			err = testClient.ApiextensionsV1().CustomResourceDefinitions().Delete(context.TODO(), crd.Name, *v1.NewDeleteOptions(0))
			assert.Nil(t, err)
		}
	})

	t.Run("CrdsNegative", func(t *testing.T) {
		err := initWhitelist()
		assert.Nil(t, err)
		testClient := kubernetes.NewFakeClientset()

		config := Config{
			Kube: testClient,
		}

		err = kubernetes.CreateCrdForTest("notaproviderpodatall", testClient)
		assert.Nil(t, err)

		result, err := deploymentHttpsCerts(context.Background(), &config)
		assert.Nil(t, err)
		assert.Equal(t, result, ResultNo)
	})
}

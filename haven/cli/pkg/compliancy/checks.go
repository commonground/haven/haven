// Copyright © VNG Realisatie 2019-2024
// Licensed under EUPL v1.2

package compliancy

import "context"

type checkFunc func(ctx context.Context, config *Config) (Result, error)

// checkFuncMapping maps checks.yaml id's to golang methods.
var checkFuncMapping = map[string]checkFunc{
	"havenversion":      fundamentalHavenVersion,
	"clusteradmin":      fundamentalClusterAdmin,
	"multiaz":           infraMultiAZ,
	"hamasters":         infraMultiMaster,
	"haworkers":         infraMultiWorker,
	"nodehardening":     infraSecuredNodes,
	"privatenetworking": infraPrivateTopology,
	"kubernetesversion": clusterVersion,
	"rbac":              clusterRBAC,
	"basicauth":         clusterBasicAuth,
	"rwxvolumes":        clusterVolumes,
	"cncf":              externalCNCF,
	"autocerts":         deploymentHttpsCerts,
	"logs":              deploymentLogAggregation,
	"metrics":           deploymentMetricsServer,
	"cis":               suggestionExternalCIS,
	"kubescape":         suggestionExternalKubescape,
	"shavalidation":     shaValidation,
}

// Checks contain all compliancy and suggested checks sourced from checks.yaml.
type Checks struct {
	CompliancyChecks []Check
	SuggestedChecks  []Check
}

// Check represents a single Compliancy or Suggested check.
type Check struct {
	Name      string
	Label     string
	Category  Category
	Rationale string
	Result    Result `json:",omitempty"`
}

func (c Check) Exec(ctx context.Context, config *Config) (Result, error) {
	return checkFuncMapping[c.Name](ctx, config)
}

---
apiVersion: batch/v1
kind: Job
metadata:
  name: {{ .Name }}
spec:
  template:
    metadata:
      labels:
        app: {{ .Name }}
    spec:
      serviceAccountName: {{ .ServiceAccountName }}
      containers:
      - command: ["/bin/sh", "-c"]
        args:
        - kubescape scan --keep-local
        image: quay.io/armosec/kubescape:latest
        name: {{ .Name }}
        resources: {}
        securityContext:
          allowPrivilegeEscalation: false
          seccompProfile:
            type: RuntimeDefault
          capabilities:
            drop:
            - ALL
        volumeMounts:
        - mountPath: /var/lib/kubelet
          name: var-lib-kubelet
          readOnly: true
        - mountPath: /etc/systemd
          name: etc-systemd
          readOnly: true
        - mountPath: /lib/systemd
          name: lib-systemd
          readOnly: true
        - mountPath: /etc/kubernetes
          name: etc-kubernetes
          readOnly: true
      hostPID: true
      restartPolicy: Never
      volumes:
      - hostPath:
          path: /var/lib/kubelet
        name: var-lib-kubelet
      - hostPath:
          path: /etc/systemd
        name: etc-systemd
      - hostPath:
          path: /lib/systemd
        name: lib-systemd
      - hostPath:
          path: /etc/kubernetes
        name: etc-kubernetes

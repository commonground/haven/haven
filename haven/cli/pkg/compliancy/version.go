package compliancy

import (
	"github.com/spf13/cobra"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
)

func NewCmdVersion() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "version",
		Short: "Prints the Haven CLI version",
		Run: func(cmd *cobra.Command, args []string) {
		},
	}

	cmd.PersistentPreRun = func(cmd *cobra.Command, args []string) {
		logging.WriteHavenHeader()
	}

	return cmd
}

package kubernetes

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	corev1 "k8s.io/api/core/v1"
)

func TestPod(t *testing.T) {
	ns := "cmdonpod-test"
	cmd := "unittests-cmdonpod_test.go"
	nodeName := "someNodeName"
	globalInterval := 1 * time.Millisecond
	testClient := NewFakeClientset()
	errorText := "pod"
	staticTextFromExecPod := "FakeClientDoesNotExecute"

	t.Run("RunWithoutMaster", func(t *testing.T) {
		done := make(chan struct{})

		go func() {
			err := SetPodStatusToRunning(done, testClient, ns)
			assert.Nil(t, err)
		}()

		sut, err := ExecCmd(context.Background(), testClient, cmd,
			WithNamespace(ns),
			WithTickInterval(globalInterval))
		close(done)

		assert.Nil(t, err)
		assert.Contains(t, sut, staticTextFromExecPod)
	})

	t.Run("CannotFindAnyMasterNode", func(t *testing.T) {
		sut, err := ExecCmd(context.Background(), testClient, cmd,
			WithNamespace(ns),
			WithMaster(),
			WithTickInterval(globalInterval))

		assert.NotNil(t, err)
		assert.Equal(t, "", sut)
		assert.Contains(t, err.Error(), errorText)
	})

	t.Run("MasterNodeFound", func(t *testing.T) {
		testClient = NewFakeClientset()
		taints := []corev1.Taint{
			{
				Key:       "testKey",
				Value:     "testValue",
				Effect:    "testEffect",
				TimeAdded: nil,
			},
		}

		labels := map[string]string{"kubernetes.io/role": "master"}
		err := CreateNodeForTest(nodeName, taints, labels, testClient)
		assert.Nil(t, err)

		done := make(chan struct{})

		go func() {
			err := SetPodStatusToRunning(done, testClient, ns)
			assert.Nil(t, err)
		}()

		sut, err := ExecCmd(context.Background(), testClient, cmd,
			WithNamespace(ns),
			WithTickInterval(globalInterval),
			WithMaster())

		close(done)
		assert.Nil(t, err)
		assert.Contains(t, sut, staticTextFromExecPod)
	})

	t.Run("MasterNodeFoundLabelMaster", func(t *testing.T) {
		testClient = NewFakeClientset()
		taints := []corev1.Taint{
			{
				Key:       "testKey",
				Value:     "testValue",
				Effect:    "testEffect",
				TimeAdded: nil,
			},
		}

		labels := map[string]string{"node-role.kubernetes.io/master": "exists"}
		err := CreateNodeForTest(nodeName, taints, labels, testClient)
		assert.Nil(t, err)

		done := make(chan struct{})

		go func() {
			err := SetPodStatusToRunning(done, testClient, ns)
			assert.Nil(t, err)
		}()

		sut, err := ExecCmd(context.Background(), testClient, cmd,
			WithNamespace(ns),
			WithTickInterval(globalInterval),
			WithMaster())

		close(done)

		assert.Nil(t, err)
		assert.Contains(t, sut, staticTextFromExecPod)
	})

	t.Run("MasterNodeFoundLabelControlPlane", func(t *testing.T) {
		testClient = NewFakeClientset()
		taints := []corev1.Taint{
			{
				Key:       "testKey",
				Value:     "testValue",
				Effect:    "testEffect",
				TimeAdded: nil,
			},
		}

		labels := map[string]string{"node-role.kubernetes.io/controlplane": "exists"}
		err := CreateNodeForTest(nodeName, taints, labels, testClient)
		assert.Nil(t, err)

		done := make(chan struct{})

		go func() {
			err := SetPodStatusToRunning(done, testClient, ns)
			assert.Nil(t, err)
		}()

		sut, err := ExecCmd(context.Background(), testClient, cmd,
			WithNamespace(ns),
			WithMaster(),
			WithTickInterval(globalInterval))

		close(done)

		assert.Nil(t, err)
		assert.Contains(t, sut, staticTextFromExecPod)
	})
}

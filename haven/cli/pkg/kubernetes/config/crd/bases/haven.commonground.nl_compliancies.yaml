---
apiVersion: apiextensions.k8s.io/v1
kind: CustomResourceDefinition
metadata:
  annotations:
    controller-gen.kubebuilder.io/version: v0.11.1
  creationTimestamp: null
  name: compliancies.haven.commonground.nl
spec:
  group: haven.commonground.nl
  names:
    kind: Compliancy
    listKind: CompliancyList
    plural: compliancies
    singular: compliancy
  scope: Cluster
  versions:
  - name: v1alpha1
    schema:
      openAPIV3Schema:
        description: Compliancy is the Schema for the compliancies API
        properties:
          apiVersion:
            description: 'APIVersion defines the versioned schema of this representation
              of an object. Servers should convert recognized schemas to the latest
              internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources'
            type: string
          kind:
            description: 'Kind is a string value representing the REST resource this
              object represents. Servers may infer this from the endpoint the client
              submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds'
            type: string
          metadata:
            type: object
          spec:
            description: CompliancySpec defines the desired state of Compliancy
            properties:
              compliant:
                type: boolean
              created:
                type: string
              input:
                description: CompliancyInput defines the input for the Compliancy
                  check
                properties:
                  commandline:
                    type: string
                  kubeHost:
                    type: string
                  platform:
                    description: CompliancyPlatform defines the platform for the Compliancy
                      check
                    properties:
                      determinedPlatform:
                        type: string
                      determiningLabel:
                        type: string
                      proxy:
                        type: boolean
                    required:
                    - determinedPlatform
                    - determiningLabel
                    - proxy
                    type: object
                required:
                - commandline
                - kubeHost
                - platform
                type: object
              output:
                description: CompliancyOutput defines the output for the Compliancy
                  check
                properties:
                  compliancyChecks:
                    description: CompliancyChecks contains the results of the checks
                    properties:
                      results:
                        items:
                          description: CompliancySummary contains the result and details
                            of a check
                          properties:
                            category:
                              type: string
                            label:
                              type: string
                            name:
                              type: string
                            rationale:
                              type: string
                            result:
                              type: string
                          required:
                          - category
                          - label
                          - name
                          - rationale
                          - result
                          type: object
                        type: array
                      summary:
                        description: CompliancySummary contains the summary of the
                          checks
                        properties:
                          failed:
                            type: integer
                          passed:
                            type: integer
                          skipped:
                            type: integer
                          total:
                            type: integer
                          unknown:
                            type: integer
                        required:
                        - failed
                        - passed
                        - skipped
                        - total
                        - unknown
                        type: object
                    required:
                    - results
                    - summary
                    type: object
                  config:
                    description: ComplicacyConfig contains the configuration for optional
                      checks
                    properties:
                      CIS:
                        type: boolean
                      CNCF:
                        type: boolean
                      kubescape:
                        type: boolean
                    required:
                    - CIS
                    - CNCF
                    - kubescape
                    type: object
                  havenCompliant:
                    type: boolean
                  startTS:
                    type: string
                  stopTS:
                    type: string
                  suggestedChecks:
                    description: CompliancySuggestedChecks contains the suggested
                      checks results
                    properties:
                      results:
                        items:
                          description: CompliancySummary contains the result and details
                            of a check
                          properties:
                            category:
                              type: string
                            label:
                              type: string
                            name:
                              type: string
                            rationale:
                              type: string
                            result:
                              type: string
                          required:
                          - category
                          - label
                          - name
                          - rationale
                          - result
                          type: object
                        type: array
                    required:
                    - results
                    type: object
                  version:
                    type: string
                required:
                - compliancyChecks
                - config
                - havenCompliant
                - startTS
                - stopTS
                - suggestedChecks
                - version
                type: object
              version:
                type: string
            required:
            - compliant
            - created
            - input
            - output
            - version
            type: object
        required:
        - spec
        type: object
    served: true
    storage: true

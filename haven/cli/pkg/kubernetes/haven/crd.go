package haven

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes/v1alpha1"
	rbacApiV1 "k8s.io/api/rbac/v1"
	apiextensionv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	"k8s.io/client-go/rest"
	"k8s.io/kubectl/pkg/scheme"
	rbachelper "k8s.io/kubernetes/pkg/apis/rbac/v1"
	"sigs.k8s.io/yaml"
)

const (
	ClusterRoleName        string = "haven-compliancy-view"
	ClusterRoleBindingName string = "haven-compliancy-view-authenticated"
	Plural                 string = "compliancies"
)

type V1Alpha1 interface {
	List() (*v1alpha1.CompliancyList, error)
	Get(name string) (*v1alpha1.Compliancy, error)
	Create(compliancy *v1alpha1.Compliancy) (*v1alpha1.Compliancy, error)
	CreateHavenResource(output string, input v1alpha1.CompliancyInput, compliant bool) (*v1alpha1.Compliancy, error)
	CreateHavenDefinition() (bool, error)
	GetDefinition(name string) (*apiextensionv1.CustomResourceDefinition, error)
}

type V1Alpha1Client struct {
	Client    rest.Interface
	Clientset *kubernetes.Clientset
}

func NewCrdConfig(clientset *kubernetes.Clientset) (V1Alpha1, error) {
	if err := v1alpha1.AddToScheme(scheme.Scheme); err != nil {
		return nil, err
	}

	crdConfig := *clientset.RestConfig()
	crdConfig.ContentConfig.GroupVersion = &v1alpha1.GroupVersion
	crdConfig.APIPath = "/apis"
	crdConfig.NegotiatedSerializer = serializer.NewCodecFactory(scheme.Scheme)
	crdConfig.UserAgent = rest.DefaultKubernetesUserAgent()

	client, err := rest.UnversionedRESTClientFor(&crdConfig)
	if err != nil {
		return nil, err
	}

	return &V1Alpha1Client{
		Client:    client,
		Clientset: clientset,
	}, nil
}

func (c *V1Alpha1Client) CreateHavenResource(rawOutput string, input v1alpha1.CompliancyInput, compliant bool) (*v1alpha1.Compliancy, error) {
	var output v1alpha1.CompliancyOutput

	if err := json.Unmarshal([]byte(rawOutput), &output); err != nil {
		return nil, err
	}

	return &v1alpha1.Compliancy{
		TypeMeta: metav1.TypeMeta{
			Kind:       v1alpha1.Kind,
			APIVersion: v1alpha1.GroupVersion.String(),
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: fmt.Sprintf("haven-%s", time.Now().UTC().Format("20060102-150405")),
		},
		Spec: v1alpha1.CompliancySpec{
			Compliant: compliant,
			Created:   time.Now().UTC().String(),
			Version:   fmt.Sprintf("Haven %s", v1alpha1.GroupVersion.Version),
			Output:    output,
			Input:     input,
		},
	}, nil
}

func (c *V1Alpha1Client) Create(compliancy *v1alpha1.Compliancy) (*v1alpha1.Compliancy, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	compl, err := json.Marshal(compliancy)
	if err != nil {
		return nil, err
	}

	var result v1alpha1.Compliancy

	err = c.Client.
		Post().
		Resource(v1alpha1.Plural).
		Body(compl).
		Do(ctx).
		Into(&result)

	return &result, err
}

func (c *V1Alpha1Client) List() (*v1alpha1.CompliancyList, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	result := v1alpha1.CompliancyList{}
	err := c.Client.
		Get().
		Resource(v1alpha1.Plural).
		Do(ctx).
		Into(&result)

	return &result, err
}

func (c *V1Alpha1Client) Get(name string) (*v1alpha1.Compliancy, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	result := v1alpha1.Compliancy{}
	err := c.Client.
		Get().
		Name(name).
		Resource(v1alpha1.Plural).
		Do(ctx).
		Into(&result)

	return &result, err
}

// CreateHavenDefinition retrieves the Haven CRD and will deploy it to the cluster when it's not installed yet.
func (c *V1Alpha1Client) CreateHavenDefinition() (bool, error) {
	ctx := context.TODO()

	var (
		created bool
		crd     apiextensionv1.CustomResourceDefinition
	)

	content, err := kubernetes.CustomResourceDefinitions.ReadFile("config/crd/bases/haven.commonground.nl_compliancies.yaml")
	if err != nil {
		return false, fmt.Errorf("failed to read CRD, this shouldn't happen: %w", err)
	}

	if err := yaml.Unmarshal(content, &crd); err != nil {
		return false, fmt.Errorf("failed to unmarshal CRD, this shouldn't happen: %w", err)
	}

	if _, err := c.GetDefinition(crd.Name); err != nil {
		if k8serrors.IsNotFound(err) {
			created = true

			_, err := c.Clientset.ApiextensionsV1().
				CustomResourceDefinitions().
				Create(ctx, &crd, metav1.CreateOptions{})
			if err != nil {
				return created, err
			}
		}
	}

	clusterRole := &rbacApiV1.ClusterRole{
		ObjectMeta: metav1.ObjectMeta{Name: ClusterRoleName},
		Rules: []rbacApiV1.PolicyRule{
			rbachelper.NewRule("get", "list", "watch").
				Groups(v1alpha1.GroupVersion.Group).
				Resources(Plural).
				RuleOrDie(),
		},
	}

	clusterRoleBinding := &rbacApiV1.ClusterRoleBinding{
		ObjectMeta: metav1.ObjectMeta{Name: ClusterRoleBindingName},
		RoleRef:    rbacApiV1.RoleRef{Kind: "ClusterRole", Name: "haven-compliancy-view"},
		Subjects: []rbacApiV1.Subject{
			{Kind: "Group", Name: "system:authenticated"},
		},
	}

	if _, err = c.Clientset.RbacV1().
		ClusterRoles().
		Create(ctx, clusterRole, metav1.CreateOptions{}); err != nil {
		if !k8serrors.IsAlreadyExists(err) {
			return created, err
		}
	}

	if _, err = c.Clientset.RbacV1().
		ClusterRoleBindings().
		Create(ctx, clusterRoleBinding, metav1.CreateOptions{}); err != nil {
		if !k8serrors.IsAlreadyExists(err) {
			return created, err
		}
	}

	return created, nil
}

func (c *V1Alpha1Client) GetDefinition(name string) (*apiextensionv1.CustomResourceDefinition, error) {
	definition, err := c.Clientset.ApiextensionsV1().CustomResourceDefinitions().Get(context.Background(), name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}

	return definition, err
}

package handler

import (
	"bytes"
	"net/http"
	"net/url"
	"time"
)

const JsonContentType = "application/json"

func Request(u url.URL, body []byte, contentType string) (*http.Response, error) {
	if contentType == "" {
		contentType = JsonContentType
	}

	var req *http.Request
	if body != nil {
		req, _ = http.NewRequest(http.MethodPost, u.String(), bytes.NewBuffer(body))
	} else {
		req, _ = http.NewRequest(http.MethodGet, u.String(), nil)
	}

	req.Header.Set("Content-Type", contentType)
	client := &http.Client{Timeout: 30 * time.Second}
	return client.Do(req)
}

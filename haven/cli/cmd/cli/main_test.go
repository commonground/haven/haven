package main

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
	"io"
	"os"
	"testing"
)

func TestShouldPrintHavenHeaders(t *testing.T) {
	originalArgs := os.Args

	tests := []struct {
		name          string
		output        bool
		args          []string
		expectedPrint string
	}{
		{
			name:          "TestHaven",
			args:          []string{"haven"},
			output:        true,
			expectedPrint: "Haven undefined - Copyright © VNG Realisatie 2019-2023 - Licensed under the EUPL v1.2.\n",
		},
		{
			name:          "TestHavenHelp",
			args:          []string{"haven", "--help"},
			output:        true,
			expectedPrint: "Haven undefined - Copyright © VNG Realisatie 2019-2023 - Licensed under the EUPL v1.2.\n",
		},
		{
			name:          "TestHavenCompletionHelp",
			args:          []string{"haven", "completion", "--help"},
			output:        true,
			expectedPrint: "Haven undefined - Copyright © VNG Realisatie 2019-2023 - Licensed under the EUPL v1.2.\n",
		},
		{
			name:          "TestHavenCompletionBashHelp",
			args:          []string{"haven", "completion", "bash", "--help"},
			output:        true,
			expectedPrint: "Haven undefined - Copyright © VNG Realisatie 2019-2023 - Licensed under the EUPL v1.2.\n",
		},
		{
			name:          "TestHavenVersionHelp",
			args:          []string{"haven", "version", "--help"},
			output:        true,
			expectedPrint: "Haven undefined - Copyright © VNG Realisatie 2019-2023 - Licensed under the EUPL v1.2.\n",
		},
		{
			name:          "TestHavenCompletionBash",
			args:          []string{"haven", "completion", "bash"},
			output:        false,
			expectedPrint: "Haven undefined - Copyright © VNG Realisatie 2019-2023 - Licensed under the EUPL v1.2.\n",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			oldStdout := os.Stdout
			oldStderr := os.Stderr

			rStdout, wStdout, _ := os.Pipe()
			rStderr, wStderr, _ := os.Pipe()

			os.Stdout = wStdout
			os.Stderr = wStderr

			defer func() {
				os.Stdout = oldStdout
				os.Stderr = oldStderr

				wStdout.Close()
				wStderr.Close()

				stdoutBytes, _ := io.ReadAll(rStdout)
				stderrBytes, _ := io.ReadAll(rStderr)

				output := string(stdoutBytes) + string(stderrBytes)
				if test.output {
					assert.Contains(t, output, test.expectedPrint)
				} else {
					assert.NotContains(t, output, test.expectedPrint)
				}
			}()

			os.Args = test.args
			defer func() {
				os.Args = originalArgs
			}()

			main()
		})
	}

}

func TestShouldPrintHavenHeadersLong(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping TestShouldPrintHavenHeadersLong in short mode")
	}

	originalArgs := os.Args

	tests := []struct {
		name          string
		output        bool
		args          []string
		expectedPrint string
	}{
		{
			name:          "TestHavenCheckJson",
			args:          []string{"haven", "check", "--output=json"},
			output:        false,
			expectedPrint: "Haven undefined - Copyright © VNG Realisatie 2019-2023 - Licensed under the EUPL v1.2.\n",
		},

		{
			name:          "TestHavenHelpCheck",
			args:          []string{"haven", "check", "--help"},
			output:        true,
			expectedPrint: "Haven undefined - Copyright © VNG Realisatie 2019-2023 - Licensed under the EUPL v1.2.\n",
		},
		{
			name:          "TestHavenHelpCheckJson",
			args:          []string{"haven", "check", "--help", "--output=json"},
			output:        true,
			expectedPrint: "Haven undefined - Copyright © VNG Realisatie 2019-2023 - Licensed under the EUPL v1.2.\n",
		},
		{
			name:          "TestHavenCheck",
			args:          []string{"haven", "check"},
			output:        true,
			expectedPrint: "Haven undefined - Copyright © VNG Realisatie 2019-2023 - Licensed under the EUPL v1.2.\n",
		},
		{
			name:          "TestHavenHelpCheckText",
			args:          []string{"haven", "check", "--help", "--output=text"},
			output:        true,
			expectedPrint: "Haven undefined - Copyright © VNG Realisatie 2019-2023 - Licensed under the EUPL v1.2.\n",
		},
		{
			name:          "TestHavenHelpCheckOutPutText",
			args:          []string{"haven", "check", "--output=text"},
			output:        true,
			expectedPrint: "Haven undefined - Copyright © VNG Realisatie 2019-2023 - Licensed under the EUPL v1.2.\n",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			logging.OutputFormat = nil

			oldStdout := os.Stdout
			oldStderr := os.Stderr

			rStdout, wStdout, _ := os.Pipe()
			rStderr, wStderr, _ := os.Pipe()

			os.Stdout = wStdout
			os.Stderr = wStderr

			defer func() {
				os.Stdout = oldStdout
				os.Stderr = oldStderr

				wStdout.Close()
				wStderr.Close()

				stdoutBytes, _ := io.ReadAll(rStdout)
				stderrBytes, _ := io.ReadAll(rStderr)

				output := string(stdoutBytes) + string(stderrBytes)
				if test.output {
					assert.Contains(t, output, test.expectedPrint)
				} else {
					assert.NotContains(t, output, test.expectedPrint)
				}
			}()

			os.Args = test.args
			defer func() {
				os.Args = originalArgs
			}()

			cmd := NewCmdHaven()
			err := cmd.Execute()
			assert.Nil(t, err)
		})
	}

}

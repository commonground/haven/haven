# Changes for Haven v12.0.0

To move with the market and update Haven it has been decided to upgrade the version to `v12.0.0` which brings breaking changes to some core components.

## Changes

[Branch](https://gitlab.com/commonground/haven/haven/-/tree/feat/releasev12/)

[Issue board](https://gitlab.com/commonground/haven/haven/-/issues/656)

### 

Link: https://gitlab.com/commonground/haven/haven/-/merge_requests/742


### Add flatcar as a verified secure flavor for kubernetes: 

Link: https://gitlab.com/commonground/haven/haven/-/merge_requests/731

Flatcar is a minimalistic Linux Distribution supported by large cloud vendors.

Since [flatcar](https://www.flatcar.org/) has a secure by design approach the Haven team feels it matches with the demands set for node security.

### Removes the LoadBalancer check

https://gitlab.com/commonground/haven/haven/-/merge_requests/737

The loadbalancer check has long been a staple of the Haven compliancy check but it has been decided to remove it.
Kubernetes has grown from a platform to support most web based applications to edge, queue based nodes and many more.
Some of these do not require any ingress or loadbalancer since the cluster cannot be reached and should not be reached.
For example when using a queue from a cloud provider or on prem it is totally viable to have your Kubernetes cluster run in the background and taking things of the queue instead of being called through an api.

The other factor in this decision has been the growing number of private tunnels see for example [cloudflare private tunnel](https://developers.cloudflare.com/cloudflare-one/connections/connect-networks/private-net/).
In this way a cluster can simply be exposed by using services rather than a (cloud native) loadbalancer.

### Add sha checksum

https://gitlab.com/commonground/haven/haven/-/merge_requests/743
   
It should be possible for a municipality or any user of the Haven compliancy checker to validate that the version in use has not been tempered with. 
Therefor a sha checksum check has been added. Each checksum can be found along side the actual cli and can be verified by any user.

A remote sha is part of the releases that is currently checked for up to date versions of haven which will now also validate the sha.

### Major version upgrades

Golang has been upgraded to version 1.22.2 which is the latest and all Kubernetes apis to version 0.30.0 which is the most recent version.

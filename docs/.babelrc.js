module.exports = {
  presets: ["next/babel"],
  plugins: [
    [
      "styled-components",
      {
        ssr: true,
      },
    ],
    [
      "inline-react-svg",
      {
        caseSensitive: true,
        svgo: {
          plugins: [
            {
              name: "preset-default",
              params: {
                overrides: {
                  // customize options for plugins included in preset
                  cleanupIDs: {
                    minify: false,
                    preserve: ["dashedlines"],
                  },
                },
              },
            },
            "removeDimensions",
          ],
        },
      },
    ],
  ],
};

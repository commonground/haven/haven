---
title: "Red Hat OpenShift Container Platform op Azure"
path: "/aan-de-slag/openshift-op-azure"
---

## Referentie Implementatie: Haven op Red Hat OCP op Azure

Deze referentie implementatie verschilt van de [Azure Red Hat OpenShift referentie
implementatie](/techniek/aan-de-slag/aro) in het
feit dat onderstaande tekst een OpenShift cluster opbouwt dat door uzelf beheerd wordt,
en de andere referentie implementatie een cluster opbouwt dat door Microsoft als managed
service aangeboden wordt.

### Voorwaarden

Stap 1: Configureer de Azure account zoals beschreven in de [OpenShift
    documentatie](https://docs.openshift.com/container-platform/4.6/installing/installing_azure/installing-azure-account.html), en zorg dat
- er geen limieten op vCPUs geconfigureerd zijn (of de limiet op minimaal 40 staat)
- er een publieke DNS zone aanwezig is in Azure om de DNS records voor het cluster in te maken
- er een service principal aan is gemaakt met de juiste permissies


Stap 2: Verzamel de benodigde informatie omtrent het Azure account. Onderstaande UUIDs zin in ieder geval zijn nodig[^1]:
- de UUID van de subscriptie binnen Azure
- de UUID van de tenant binnen Azure
- de UUID van de service principal en het bijbehorende secret

### Basis installatie

Stap 3: Download de OpenShift installer vanaf [cloud.redhat.com](https://cloud.redhat.com/openshift/install/azure/installer-provisioned) en pak het bestand uit naar de lokale schijf

Stap 4a: Om een cluster met de standaard instellingen aan te maken, voer het volgende commando
    uit[^2]. Het installatie bestand vraagt nu om de informatie die we hebben verzameld bij
    de vorige stap:

    ./openshift-install create cluster

Stap 4b: Om een cluster aan te maken met aangepaste instellingen, voer het volgende commando uit. Ook hier vraagt het installatie bestand om de informatie uit stap 2. Dit commando genereert een ``install-config.yaml`` bestand, dat bijvoorbeeld aangepast kan worden om bijvoorbeeld workers uit te rollen met een grotere maat, of een groter aantal.

Omdat de Haven implementatie uitgaat van geïntegreerd log aggregation, raden wij aan om
voor de workers minimaal Standard_D4s_v3 instances te gebruiken. Hoe dit precies werkt,
staat op deze
[pagina](https://docs.openshift.com/container-platform/4.6/installing/installing_azure/installing-azure-customizations.html#installation-azure-config-yaml_installing-azure-customizations).

    ./openshift-install create install-config

Na de aanpassing van het ``install-config.yaml`` bestand, voert u het installatie
bestand nogmaals uit (er vanuit gaande dat ``install-config.yaml`` in dezelfde directory
staat als de installer):

    ./openshift-install create cluster --dir=.

Onafhankelijk van of u stap 4a of 4b gevolgd hebt, duurt de installatie circa 45
minuten. Het installatieproces eindigt het het weergeven van de URL van de OpenShift
console, de API URL, en het kubeadmin password.

Hiermee hebt u een werkend OpenShift cluster, dat al voor het grootste deel
voldoet aan Haven.


### Naar volledige Haven compliancy
Om volledig compliant te zijn, heeft het OpenShift cluster nog twee aanpassingen nodig:
er moet een log aggregator worden geïnstalleerd, en er moet RWX storage beschikbaar
worden gemaakt.

#### Log aggregator
Red Hat OpenShift wordt geleverd met geïntegreerde logging stack, maar die wordt niet
automatisch geïnstalleerd om gebruikers de mogelijkheid te geven met een andere logging
stack te werken.

In deze referentie implementatie installeren we de standaard logging stack achteraf, om
compliancy met Haven te bereiken.

Om de logging stack te installeren, volgen we de
[handleiding](https://docs.openshift.com/container-platform/4.6/logging/cluster-logging-deploying.html).

Hierin lezen we dat we eerst de ElasticSearch operator op het cluster moeten
installeren, en vervolgens de cluster-logging operator.

Vervolgens creëren we een instance ClusterLogging. De verdere installatie van Fluentd,
ElasticSearch en het Kibana dashboard gaat vanzelf. Na deze installatie is er een log
aggregator beschikbaar, compleet met zoekmachine en dashboard.

#### RWX storage
Haven compliancy vraagt tot slot om RWX storage, dat wil zeggen: storage die door
meerdere pods tegelijk gebruikt kan worden om op te schrijven. Dit is niet op alle
platformen zonder meer beschikbaar.

Op Azure kunnen we RWX storage implementeren met de Azure File service. Hiervoor moeten
we eenmalig een aantal stappen doorlopen. Eerst maken we een storage account aan binnen
Azure:

```bash
# Vul hier de namen van een resource group, een locatie en een storage account in.
# Hoewel dit dezelfde resource group kan zijn als de resource group waarin OpenShift
# draait, raden we dit af: het verwijderen van het cluster met de installer verwijdert
# dan ook de storage account. Dit is niet altijd wenselijk.
export AZURE_FILES_RESOURCE_GROUP=
export AZURE_STORAGE_ACCOUNT_NAME=
export LOCATION=

az storage account create \
    --name $AZURE_STORAGE_ACCOUNT_NAME \
    --resource-group $AZURE_FILES_RESOURCE_GROUP \
    --kind StorageV2 \
    --sku Standard_LRS
```

Vervolgens wijzen we de juiste rollen rol toe aan de service principal binnen deze
resource group in Azure:
```bash
# Vul de ID in van de service principal die gebruikt is om het OpenShift cluster te
# installeren
export AZURE_SERVICE_PRINCIPAL_ID=
az role assignment create --role Contributor --assignee $AZURE_SERVICE_PRINCIPAL_ID -g $AZURE_FILES_RESOURCE_GROUP
```

Vervolgens maken we binnen OpenShift een rol aan om met Azure te kunnen communiceren:
```bash
# Vul hier het wachtwoord van de kubeadmin in, en de URL van de OpenShift API. Beide
# worden weergegeven nadat de installatie voltooid is.
export PASSWORD=$kubeadmin_pw
export APISERVER=$openshift_api_server

oc login -u kubeadmin -p $PASSWORD $APISERVER

oc create clusterrole azure-secret-reader \
    --verb=create,get \
    --resource=secrets

oc adm policy add-cluster-role-to-user azure-secret-reader system:serviceaccount:kube-system:persistent-volume-binder
```

En uiteindelijk maken we de daadwerkelijke storage class aan:
```bash
# Vul hieronder wederom de juist waarden in voor de naam van de storage account en de
# resource group.
cat << EOF >> azure-storageclass-azure-file.yaml
kind: StorageClass
apiVersion: storage.k8s.io/v1
metadata:
  name: azure-file
provisioner: kubernetes.io/azure-file
parameters:
  location: $LOCATION
  secretNamespace: kube-system
  skuName: Standard_LRS
  storageAccount: $AZURE_STORAGE_ACCOUNT_NAME
  resourceGroup: $AZURE_FILES_RESOURCE_GROUP
reclaimPolicy: Delete
volumeBindingMode: Immediate
EOF
oc create -f azure-storageclass-azure-file.yaml
```

Hier staat een [voorbeeld Ansible playbook](https://gitlab.com/commonground/haven/haven/-/tree/main/reference/openshift-on-azure)
om bovenstaande aanpassingen op een Azure account te doen. Dit playbook kan worden
uitgevoerd na de deployment van OpenShift zelf, en zorgt voor configuratie van de RWX
storage.

### Onderhoud

OpenShift is gebaseerd op zogenaamde operators[^3]. Heel kort gezegd zijn operators
stukjes software die in staat zijn om een ander stuk software te beheren. Door gebruik
van operators is OpenShift in staat om een groot deel van zijn eigen onderhoud te doen.

Als er een update voor een OpenShift cluster beschikbaar is, is dat zichtbaar in de
OpenShift console. Updates kunnen op een geschikt moment door een druk op de knop worden
geinstalleerd. Als een OpenShift update wordt geinstalleerd, worden ook de onderliggende
VMs automatisch geupdatet.

Bovenstaande geldt zowel voor updates tussen minor releases (bijvoorbeeld van 4.6.23
naar 4.6.24) als voor updates tussen major releases (bijvoorbeeld van 4.6.24 naar
4.7.1).

Bij het installeren van additionele operators (zie hieronder) kan worden aangegeven of
de operators zichzelf automatisch mogen updaten, of dat updates handmatig geinitieerd
moeten worden.  De keuze voor automatische of handmatige updates hangt af van
verschillende keuzes die buiten de scope van dit document vallen.

### Vervolgstappen

Ook software die op OpenShift draait, wordt vaak beheerd door een operator. Vanuit de
ingebouwde OperatorHub kan additionele software worden geinstalleerd, zoals bijvoorbeeld
een Kafka cluster of serverless functionaliteit. Ook veel software van derden wordt via
de ingebouwde OperatorHub op OpenShift aangeboden, zoals geclusterde PostgreSQL
databases, API gateways, en monitoring software.

Ga voor de OperatorHub in het menu aan de linkerzijde naar Operators -> OperatorHub.

### Verder lezen

- [De volledige OpenShift documentatie](https://docs.openshift.com/container-platform/4.7/welcome/index.html)
- [OpenShift uitproberen](https://www.openshift.com/try)
- [Meer leren met hands-on labs](https://learn.openshift.com/)


[^1]: De UUIDs zijn eenvoudig te vinden met het `az` commando
  line interface voor Azure. `az account show` geeft onder 'id' het subscription id, en
  onder 'tenantId' het tenant id. Tijdens het aanmaken van de service principal,
  [hier](https://docs.openshift.com/container-platform/4.7/installing/installing_azure/installing-azure-account.html#installing-azure-account)
  beschreven, moeten de UUID en het secret van de service principal worden genoteerd.

[^2]: De installer moet worden uitgevoerd vanaf een Linux of MacOS machine.

[^3]: Zie ook: https://www.redhat.com/en/topics/containers/what-is-a-kubernetes-operator

---
title: "Previder Kubernetes Engine (PKE)"
path: "/aan-de-slag/previder"
---

_Referentie Implementatie: Haven on Previder Kubernetes Engine_

<img src="/technique/content/documentation/eenvoudig-beheer/aan-de-slag/previder/previder.png" style="width: 250px" />

De voorbeeldconfiguratie is [beschikbaar als OpenTofu/Terraform code](https://gitlab.com/commonground/haven/haven/-/tree/main/reference/previder)

### Previder Kubernetes Engine (PKE)

[Previder Kubernetes Engine](https://previder.nl/diensten/kubernetes-as-a-service) wordt uitsluitend in eigen Nederlandse datacenters gehost en beheerd, waarbij redundantie over [meerdere datacenterlocaties](https://previder.nl/diensten/datacenter) hoge beschikbaarheid garandeert.

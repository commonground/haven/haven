---
title: "OpenStack (Kops)"
path: "/aan-de-slag/openstack"
---

_Referentie Implementatie: Haven op OpenStack met Kops_

Haven is eenvoudig op OpenStack te deployen waarbij we gebruik maken van [Kops](https://kops.sigs.k8s.io/). Dit is de Referentie Implementatie die we bij VNG Realisatie intern gebruiken.

## In gebruik nemen
De Haven repository op gitlab bevat de benodigde scripts om eenvoudig een cluster te deployen en beheren. Het geheel wordt door Haven gebruiksklaar aangeleverd in een docker container, deze kun je desgewenst ook zelf bouwen.

Voor meer informatie over de OpenStack Referentie Implementatie is er de [README](https://gitlab.com/commonground/haven/haven/-/tree/main/reference/openstack/README.md) in de repository van Haven.

## Ontwikkelen
Als ontwikkelaar kan het prettig zijn om op een development omgeving te werken. OpenStack voorziet hierin middels de Devstack. De [voorbeeldconfiguratie](https://gitlab.com/commonground/haven/haven/-/tree/main/reference/openstack/devstack) is tevens vindbaar in de Haven repository bij de OpenStack Referentie Implementatie.

---
title: "Amazon Elastic Kubernetes Service (EKS)"
path: "/aan-de-slag/aws"
---

_Referentie Implementatie: Haven op Amazon Web Services met EKS_

De voorbeeldconfiguratie is [beschikbaar als CDK code](https://gitlab.com/commonground/haven/haven/-/tree/main/reference/aws).

![Schermafbeelding van een Kubernetes-cluster op Amazon Elastic Kubernetes Service (EKS)](/technique/content/documentation/eenvoudig-beheer/aan-de-slag/aws/aws.png)

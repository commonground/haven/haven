---
videoCaption: Sfeer impressie.
fallbackYoutubeText: Uw browser ondersteund het afspelen van video niet. Bekijk deze op
fallbackYoutubeUri: https://www.youtube.com/embed/XWToxbGTHzY
---

## Terugblik eerste Haven community dag

De eerste Haven Communitydag is een feit en we kijken terug op een succesvolle bijeenkomst!
Dit markeert het begin van een spannende ontwikkeling waarbij we verder samenwerken met de markt en de overheid om de Haven-standaard voor platformonafhankelijke clouddiensten te optimaliseren en verder te ontwikkelen.

Namens het Haven-team wil ik graag alle vendoren en gemeenten bedanken voor hun bijdragen aan het programma. We hebben de dag als zeer waardevol ervaren en hebben gemerkt dat er veel positieve energie heerst rondom dit onderwerp. Dit geeft ons het vertrouwen dat we samen mooie resultaten kunnen realiseren.

Wil je nog even herinneringen ophalen? Kijk dan hieronder de video met een sfeerimpressie van de Haven community bijeenkomst.

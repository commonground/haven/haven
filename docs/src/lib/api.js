// Copyright © VNG Realisatie 2019-2024
// Licensed under EUPL v1.2
//
import fs from "fs";
import path from "path";
import matter from "gray-matter";
import markdownToHtml from "./markdownToHtml";

const publicDir = path.join(process.cwd(), "public");

export function parseMdFile(file) {
  // eslint-disable-next-line security/detect-non-literal-fs-filename
  const fileContents = fs.readFileSync(file, "utf8");
  return matter(fileContents);
}

export async function buildSectionProps(contentDir, file) {
  const { content, data } = parseMdFile(path.join(contentDir, file));
  const sectionProps = { ...data };

  if (content) {
    sectionProps.content = await markdownToHtml(content);
  }

  return sectionProps;
}

export async function buildSectionMap(contentDir, files, customName) {
  const sections = new Map();

  for (const file of files) {
    let sectionProps = await buildSectionProps(contentDir, file);

    let name = customName;

    if (!customName) {
      name = file.replace(/^\d+_/, "");
      name = name.replace(/\.md$/, "");
    }

    const [sectionName, nestedName, tooDeep] = name.split(".");
    const section = sections.get(sectionName) || {};

    if (tooDeep) {
      throw Error("Only supporting nested sections 1 deep");
    }

    if (nestedName) {
      sectionProps = { ...section, [nestedName]: { ...sectionProps } };
    }

    sections.set(sectionName, sectionProps);
  }

  return sections;
}

export async function getPageSections(page) {
  const contentDir = path.join(publicDir, page, "content");
  // eslint-disable-next-line security/detect-non-literal-fs-filename
  const files = fs.readdirSync(contentDir).filter((file) => file.match(/.md$/));
  const sections = await buildSectionMap(contentDir, files);

  const genericDir = path.join(publicDir, "generic", "content");
  // eslint-disable-next-line security/detect-non-literal-fs-filename
  const genericFiles = fs
    .readdirSync(genericDir)
    .filter((file) => file.match(/.md$/));
  const genericSections = await buildSectionMap(genericDir, genericFiles);

  return Object.fromEntries([...sections, ...genericSections]);
}

export const getFilesRecursively = (directory) =>
  // eslint-disable-next-line security/detect-non-literal-fs-filename
  fs
    .readdirSync(directory)
    .map((file) => {
      const absolute = path.join(directory, file);
      // eslint-disable-next-line security/detect-non-literal-fs-filename
      return fs.statSync(absolute).isDirectory()
        ? getFilesRecursively(absolute)
        : absolute;
    })
    .flat();

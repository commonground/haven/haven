// Copyright © VNG Realisatie 2019-2024
// Licensed under EUPL v1.2
//
import React from "react";
import { string, func } from "prop-types";
import Link from "next/link";
import {
  StyledButton,
  PrefixIcon,
  ExternalIcon,
  IconExternalLink,
} from "./index.styles";

const LinkButton = ({ href, text, prefixIcon, ...props }) => {
  const isExternal = href && href.substring(0, 4) === "http";

  if (!text || !href) return null;

  return (
    <>
      {prefixIcon && <PrefixIcon as={prefixIcon} inline />}
      {isExternal ? (
        <StyledButton
          as="a"
          href={href}
          variant="link"
          rel="noreferrer"
          target="_blank"
          {...props}
        >
          {text}
          <ExternalIcon as={IconExternalLink} inline />
        </StyledButton>
      ) : (
        <Link href={href} passHref>
          <StyledButton as="a" variant="link" {...props}>
            {text}
          </StyledButton>
        </Link>
      )}
    </>
  );
};

LinkButton.propTypes = {
  href: string,
  text: string,
  prefixIcon: func,
};

export default LinkButton;

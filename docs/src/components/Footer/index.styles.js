// Copyright © VNG Realisatie 2019-2024
// Licensed under EUPL v1.2
//
import styled from "styled-components";
import mediaQueries from "@commonground/design-system/dist/mediaQueries";
import LogoHaven from "./vng.svg";

export const Wrapper = styled.footer`
  padding: ${(p) => p.theme.tokens.spacing07} 0;
  background-image: linear-gradient(175deg, #3a3a3a 0%, #222222 100%);
`;

export const FooterContent = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  text-align: left;

  > a:not(:last-child) {
    align-items: center;
    display: flex;
    color: ${(p) => p.theme.tokens.colors.colorPaletteGray200};
  }
`;

export const List = styled.ul`
  display: flex;
  flex-direction: column;
  padding: 0;
  margin: 0;
  list-style-type: none;

  ${mediaQueries.smUp`
    flex-direction: row;
  `}
`;

export const StyledLogoHaven = styled(LogoHaven)`
  width: 100px;
`;

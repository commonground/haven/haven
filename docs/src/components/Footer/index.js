// Copyright © VNG Realisatie 2019-2024
// Licensed under EUPL v1.2
//
import React from "react";
import Link from "next/link";
import { Container } from "src/components/Grid";
import { FooterContent, Wrapper, StyledLogoHaven } from "./index.styles";

const Footer = () => (
  <Wrapper>
    <Container>
      <FooterContent>
        <Link href="/privacyverklaring">
          <a>Privacyverklaring</a>
        </Link>
        <StyledLogoHaven />
      </FooterContent>
    </Container>
  </Wrapper>
);

export default Footer;

// Copyright © VNG Realisatie 2019-2024
// Licensed under EUPL v1.2
//
import React from "react";
import { string } from "prop-types";
import { Container } from "src/components/Grid";
import HtmlContent from "src/components/HtmlContent";
import { Section } from "./index.styles";

const Introduction = ({ content }) => (
  <Section omitArrow>
    <Container>
      <HtmlContent content={content} />
    </Container>
  </Section>
);

Introduction.propTypes = {
  content: string,
};

export default Introduction;

// Copyright © VNG Realisatie 2019-2024
// Licensed under EUPL v1.2
//
import styled from "styled-components";
import mediaQueries from "@commonground/design-system/dist/mediaQueries";
import { Container } from "src/components/Grid";
import { Col, Row } from "src/components/Grid";
import BaseSection from "src/components/Section";

export const Section = styled(BaseSection)`
  padding: 0 0 ${(p) => p.theme.tokens.spacing07};
`;

export const StyledRow = styled(Row)`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`;

export const ContentCol = styled(Col)`
  ${mediaQueries.mdUp`
    width: 66.66%;
  `}
`;
export const Background = styled.figure`
  width: 100%;
  height: 100%;
  margin: 0;
  position: absolute;
  overflow: hidden;

  > img {
    height: 100%;
    width: 100%;
    object-fit: cover;
  }
`;

export const List = styled.ul`
  display: flex;
  align-items: center;
  padding: 0;
  list-style-type: none;
  flex-wrap: wrap;
  justify-content: center;

  > * + * {
    margin-left: ${(p) => `${p.theme.tokens.spacing07}`};
  }

  ${mediaQueries.mdDown`
    > *:first-child, *:last-child {
      width 100%;
      text-align: center;
    }

    > * + * {
    margin: ${(p) => `${p.theme.tokens.spacing07}`} 0 0 0;
    }

    > li {
      min-width: 140px;
      display: flex;
      justify-content: center;
    }
  `}
`;

export const Item = styled.li`
  padding: 0;
`;

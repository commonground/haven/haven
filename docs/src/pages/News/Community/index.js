// Copyright © VNG Realisatie 2019-2024
// Licensed under EUPL v1.2
//
import { string } from "prop-types";
import { Container } from "src/components/Grid";
import { Media } from "src/styling/media";
import { Section, ContentCol, StyledRow } from "./index.styles";
import HtmlContent from "../../../components/HtmlContent";

const Community = ({ fallbackYoutubeUri, content }) => {
  return (
    <Section omitArrow>
      <Media greaterThanOrEqual="md" />
      <Container>
        <ContentCol>
          <HtmlContent content={content} />
          <iframe
            width="560"
            height="315"
            src={fallbackYoutubeUri}
            frameBorder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          ></iframe>
        </ContentCol>
        <StyledRow></StyledRow>
      </Container>
    </Section>
  );
};

Community.propTypes = {
  fallbackYoutubeText: string.isRequired,
  fallbackYoutubeUri: string.isRequired,
};

export default Community;

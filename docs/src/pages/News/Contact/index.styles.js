// Copyright © VNG Realisatie 2019-2024
// Licensed under EUPL v1.2
//
import styled from "styled-components";
import mediaQueries from "@commonground/design-system/dist/mediaQueries";
import BaseSection from "src/components/Section";
import { Col, Row } from "src/components/Grid";

export const Section = styled(BaseSection)`
  &::before {
    border-top-color: #bfc8d9;

    ${mediaQueries.mdDown`
      border-top-color: #b7c3d8;
    `}
  }
`;
export const ContentCol = styled(Col)`
  ${mediaQueries.mdUp`
    width: 66.66%;
  `}
`;

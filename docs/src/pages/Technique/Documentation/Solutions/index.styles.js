// Copyright © VNG Realisatie 2019-2024
// Licensed under EUPL v1.2
//
import styled from "styled-components";
import {
  mediaQueries,
  Button,
  SelectComponent,
} from "@commonground/design-system";

export const Container = styled.div`
  display: flex;
  padding: ${(p) => p.theme.tokens.spacing07} ${(p) => p.theme.tokens.spacing05};
  background: #eeeeee;
  border-radius: 8px;

  ${mediaQueries.smDown`
    flex-direction: column;
    padding: ${(p) => p.theme.tokens.spacing05};

    > * + * {
      margin-top: ${(p) => p.theme.tokens.spacing02};
    }
  `}
`;

export const StyledButton = styled(Button)`
  border-right: 1px solid #afb2b7;

  ${(p) =>
    p.isActive &&
    `
    background-color: rgb(11, 113, 161);
    color: #ffffff;

    :hover, :focus {
      background-color: rgba(11, 113, 161, 0.9);
      color: #ffffff;
      z-index: 1;
    }
  `}

  :focus {
    margin-right: -1px;
  }
`;

export const StyledSelectComponent = styled(SelectComponent)`
  margin-left: ${(p) => p.theme.tokens.spacing06};
  flex: 1;
  width: 100%;

  ${mediaQueries.smDown`
    margin-left: 0;
  `}

  > div:last-of-type > div {
    max-height: 328px;
  }
`;

export const CardWrapper = styled.div`
  display: flex;
  margin-bottom: ${(p) => p.theme.tokens.spacing05};
  border-bottom: 1px solid #bdbdbd;
  padding: ${(p) => p.theme.tokens.spacing06} ${(p) => p.theme.tokens.spacing06}
    ${(p) => p.theme.tokens.spacing05} 0;
`;

export const ContentWrapper = styled.div`
  width: 75%;

  > h3 {
    display: inline-block;
    margin-top: 0;
  }

  > p {
    margin-bottom: ${(p) => p.theme.tokens.spacing03};
  }
`;

export const ImageWrapper = styled.div`
  flex: 1;
  display: flex;
  align-items: flex-start;
  flex-direction: row-reverse;

  > img {
    width: 80px;
  }
`;

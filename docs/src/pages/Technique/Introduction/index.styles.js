// Copyright © VNG Realisatie 2019-2024
// Licensed under EUPL v1.2
//
import styled from "styled-components";
import mediaQueries from "@commonground/design-system/dist/mediaQueries";
import BaseSection from "src/components/Section";

export const Section = styled(BaseSection)`
  padding: ${(p) => p.theme.tokens.spacing09} 0;
  background-color: #252525;
  background-image: url("/technique/content/intro-bg-pattern.svg");
  background-repeat: repeat;
  color: #ffffff;

  ${mediaQueries.mdUp`
    padding: ${(p) => p.theme.tokens.spacing10} 0;
  `}
`;

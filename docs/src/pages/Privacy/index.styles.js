// Copyright © VNG Realisatie 2019-2024
// Licensed under EUPL v1.2
//
import styled from "styled-components";
import mediaQueries from "@commonground/design-system/dist/mediaQueries";
import BaseSection from "src/components/Section";

export const Section = styled(BaseSection)`
  padding: ${(p) => p.theme.tokens.spacing09} 0;

  ${mediaQueries.mdUp`
    padding: ${(p) => p.theme.tokens.spacing10} 0;
  `}
`;

export const Subtitle = styled.p`
  font-size: ${(p) => p.theme.tokens.fontSizeXLarge};
  line-height: 125%;
  margin: 1.5rem 0 4rem;
`;

export const CollapsibleWrapper = styled.div`
  margin-top: ${(p) => p.theme.tokens.spacing06};
  border-bottom: 1px solid ${(p) => p.theme.colorCollapsibleBorder};

  > * {
    border-top: 1px solid ${(p) => p.theme.colorCollapsibleBorder};
    padding: ${(p) =>
      `${p.theme.tokens.spacing05} ${p.theme.tokens.spacing03}`};
  }
`;

export const CollapsibleTitle = styled.p`
  margin-bottom: 0;
`;

export const CollapsibleBody = styled.p`
  margin: 0;
  padding-top: ${(p) => p.theme.tokens.spacing05};
`;

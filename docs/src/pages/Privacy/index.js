// Copyright © VNG Realisatie 2019-2024
// Licensed under EUPL v1.2
//
import Head from "next/head";
import Header from "src/components/Header";
import Footer from "src/components/Footer";
import React from "react";
import Collapsible from "@commonground/design-system/dist/components/Collapsible";
import { object } from "prop-types";
import { Container } from "src/components/Grid";
import HtmlContent from "src/components/HtmlContent";

import {
  CollapsibleBody,
  CollapsibleTitle,
  CollapsibleWrapper,
  Section,
  Subtitle,
} from "./index.styles";

const Privacy = ({ sections }) => {
  const chapters = Object.entries(sections.chapters)
    .filter(([key]) => key.substring(0, 7) === "chapter")
    .map(([, chapter]) => chapter);

  return (
    <>
      <Head>
        <title>Haven - Privacyverklaring</title>
        <meta name="Privacyverklaring" content={sections.meta.description} />
      </Head>

      <Header />

      <Section omitArrow>
        <Container>
          <h1>{sections.introduction.title}</h1>
          <Subtitle>{sections.introduction.subtitle}</Subtitle>
          <HtmlContent content={sections.introduction.content} />

          {chapters.length &&
            chapters.slice(0, 3).map((chapter) => (
              <React.Fragment key={chapter.content.slice(4, 6)}>
                <HtmlContent content={chapter.content} />

                {chapter.collapsibles && (
                  <CollapsibleWrapper>
                    {chapter.collapsibles.map((collapsible) => (
                      <Collapsible
                        key={collapsible.title}
                        title={
                          <CollapsibleTitle>
                            {collapsible.title}
                          </CollapsibleTitle>
                        }
                        ariaLabel={collapsible.ariaLabel}
                      >
                        <CollapsibleBody
                          as={collapsible.element}
                          dangerouslySetInnerHTML={{ __html: collapsible.body }}
                        ></CollapsibleBody>
                      </Collapsible>
                    ))}
                  </CollapsibleWrapper>
                )}
              </React.Fragment>
            ))}

          {chapters.length &&
            chapters
              .slice(3)
              .map((chapter) => (
                <HtmlContent
                  key={chapter.content.slice(4, 6)}
                  content={chapter.content}
                />
              ))}
        </Container>
      </Section>

      <Footer />
    </>
  );
};

Privacy.propTypes = {
  sections: object.isRequired,
};

export default Privacy;

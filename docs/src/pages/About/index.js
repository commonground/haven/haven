// Copyright © VNG Realisatie 2019-2024
// Licensed under EUPL v1.2
//
import React from "react";
import { object } from "prop-types";
import Head from "next/head";
import Header from "src/components/Header";
import News from "src/components/NewsSection";
import Footer from "src/components/Footer";
import Introduction from "./Introduction";
import Background from "./Background";
import Commonground from "./Commonground";
import Publications from "./Publications";

const About = ({ sections }) => (
  <div>
    <Head>
      <title>Haven - Over</title>
      <meta name="description" content={sections.meta.description} />
    </Head>

    <Header />

    <main>
      <Introduction {...sections.introduction} />
      <Background {...sections.background} />
      <Commonground {...sections.commonground} />
      <Publications {...sections.publications} />
      <News {...sections.news} />
    </main>

    <Footer />
  </div>
);

About.propTypes = {
  sections: object.isRequired,
};

export default About;

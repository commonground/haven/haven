// Copyright © VNG Realisatie 2019-2024
// Licensed under EUPL v1.2
//
import React from "react";
import { string } from "prop-types";
import { Container } from "src/components/Grid";
import HtmlContent from "src/components/HtmlContent";
import { Section } from "./index.styles";

const Publications = ({ content }) => (
  <Section omitArrow>
    <Container>
      <HtmlContent content={content} />
    </Container>
  </Section>
);

Publications.propTypes = {
  content: string,
};

export default Publications;

// Copyright © VNG Realisatie 2019-2024
// Licensed under EUPL v1.2
//
import React from "react";
import { string } from "prop-types";
import { Container } from "src/components/Grid";
import HtmlContent from "src/components/HtmlContent";
import LinkButton from "src/components/LinkButton";
import { Section, LinkWrapper } from "./index.styles";

const Commonground = ({ content, linkHref, linkText }) => (
  <Section omitArrow>
    <Container>
      <HtmlContent content={content} />
      <LinkWrapper>
        <LinkButton href={linkHref} text={linkText} />
      </LinkWrapper>
    </Container>
  </Section>
);

Commonground.propTypes = {
  content: string,
  linkHref: string,
  linkText: string,
};

export default Commonground;

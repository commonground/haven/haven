// Copyright © VNG Realisatie 2019-2024
// Licensed under EUPL v1.2
//
import mediaQueries from "@commonground/design-system/dist/mediaQueries";
import styled from "styled-components";
import { Container } from "src/components/Grid";
import Section from "src/components/Section";

export const StyledSection = styled(Section)`
  overflow: hidden;
`;

export const LinkWrapper = styled.div`
  margin-bottom: ${(p) => p.theme.tokens.spacing08};
`;

export const AdoptationText = styled.p`
  text-align: center;
`;

export const List = styled.ul`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 0;
  margin: ${(p) => `${p.theme.tokens.spacing07} 0`};
  list-style-type: none;

  ${mediaQueries.smUp`
    flex-direction: row;
    align-items: unset;
    justify-content: space-evenly;
    flex-wrap: wrap;
  `}
`;

export const Item = styled.li`
  padding: 0;
  margin: ${(p) => `${p.theme.tokens.spacing05} ${p.theme.tokens.spacing07}`};
`;

export const Image = styled.img`
  display: block;
  max-width: 13rem;
  height: 3rem;
  filter: grayscale(100%);
  opacity: 0.8;

  ${mediaQueries.smUp`
    height: 4rem;
  `}

  a:focus > &, &:hover {
    filter: grayscale(0);
    opacity: 1;
  }
`;

export const QuotesContainer = styled(Container)`
  display: grid;
  gap: 1.5rem 1.5rem;
  margin-top: 10rem;
  margin-bottom: 6rem;
  position: relative;
  grid-auto-flow: column;

  ${mediaQueries.mdDown`
    grid-auto-flow: row;
    margin-top: 4rem;
    margin-bottom: 1rem;
  `}

  :before {
    content: "";
    position: absolute;
    background: #e0e4ea;
    border-radius: 120px;
    height: 240px;
    top: 3.5rem;
    left: -6rem;
    width: calc(100% + 12rem);

    ${mediaQueries.mdDown`
      top: -2rem;
      height: calc(100% + 4rem);
      left: 8%;
      width: 180px;
    `}
  }
`;

export const Figure = styled.figure`
  background: #ffffff;
  box-shadow: 0 22px 26px 0 rgba(0, 0, 0, 0.12);
  border-radius: 4px;
  margin: 0;
  padding-right: ${(p) => p.theme.tokens.spacing09};
  padding-bottom: ${(p) => p.theme.tokens.spacing09};
  padding-left: ${(p) => p.theme.tokens.spacing09};
  height: fit-content;
  z-index: 1;

  ${mediaQueries.mdDown`
    padding: ${(p) => p.theme.tokens.spacing06};
  `}

  svg {
    width: 80px;
    height: auto;
    margin-top: -${(p) => p.theme.tokens.spacing04};

    ${mediaQueries.mdDown`
      width: 48px;
      margin: 0;
    `}
  }
`;

export const Blockquote = styled.blockquote`
  margin: 0;

  > p {
    margin: ${(p) => p.theme.tokens.spacing06} 0 0;
    font-size: ${(p) => p.theme.tokens.fontSizeXLarge};
    line-height: 1.5;

    ${mediaQueries.mdDown`
      font-size: ${(p) => p.theme.tokens.fontSizeLarge};
      margin: ${(p) => p.theme.tokens.spacing04} 0 0;
    `}
  }

  figcaption {
    margin-top: ${(p) => p.theme.tokens.spacing07};
    font-size: ${(p) => p.theme.tokens.fontSizeLarge};
    font-weight: ${(p) => p.theme.tokens.fontWeightSemiBold};

    ${mediaQueries.mdDown`
      margin: ${(p) => p.theme.tokens.spacing04} 0 0;
    `}

    cite {
      display: block;
      font-weight: ${(p) => p.theme.tokens.fontWeightRegular};
      font-size: ${(p) => p.theme.tokens.fontSizeMedium};
      color: grey;
      font-style: normal;
    }
  }
`;

// Copyright © VNG Realisatie 2019-2024
// Licensed under EUPL v1.2
//
import styled from "styled-components";
import Icon from "@commonground/design-system/dist/components/Icon";
import mediaQueries from "@commonground/design-system/dist/mediaQueries";

export const List = styled.ul`
  padding: 0;
  max-width: 420px;
  margin: ${(p) =>
    `${p.theme.tokens.spacing07} auto ${p.theme.tokens.spacing05}`};
  list-style-type: none;

  ${mediaQueries.mdUp`
    display: flex;
    justify-content: space-between;
    max-width: none;
    margin-top: ${(p) => p.theme.tokens.spacing09}
  `}
`;

export const Item = styled.li`
  display: flex;
  padding: 0;
  margin-bottom: ${(p) => p.theme.tokens.spacing05};

  ${mediaQueries.smUp`
    align-items: center;
  `}

  ${mediaQueries.mdUp`
    flex-direction: column;
    max-width: 10rem;
    text-align: center;
  `}
`;

export const StyledIcon = styled(Icon)`
  flex: 0 0 auto;
  width: ${(p) => p.theme.tokens.iconSizeLarge};
  height: ${(p) => p.theme.tokens.iconSizeLarge};
  fill: ${(p) => p.theme.tokens.colorFocus};
  margin: 0;

  ${mediaQueries.smUp`
    width: ${(p) => p.theme.listIconSize};
    height: ${(p) => p.theme.listIconSize};
    margin-bottom: ${(p) => p.theme.tokens.spacing05};
  `}
`;

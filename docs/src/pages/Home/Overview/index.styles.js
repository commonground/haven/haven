// Copyright © VNG Realisatie 2019-2024
// Licensed under EUPL v1.2
//
import styled from "styled-components";
import Annulus1Icon from "./annulus-1.svg";
import Annulus2Icon from "./annulus-2.svg";
import Annulus3Icon from "./annulus-3.svg";

export const Annulus1 = styled(Annulus1Icon)`
  position: absolute;
  top: 0;
  width: 30%;
  left: 0;
`;
export const Annulus2 = styled(Annulus2Icon)`
  position: absolute;
  top: 0;
  width: 30%;
  right: 0;
`;
export const Annulus3 = styled(Annulus3Icon)`
  position: absolute;
  bottom: 0;
  width: 25%;
  right: 0;
`;

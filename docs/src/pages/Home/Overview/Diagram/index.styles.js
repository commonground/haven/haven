// Copyright © VNG Realisatie 2019-2024
// Licensed under EUPL v1.2
//
import styled from "styled-components";
import mediaQueries from "@commonground/design-system/dist/mediaQueries";

export const Wrapper = styled.div`
  position: relative;
  margin: 4rem 0 10rem;

  ${mediaQueries.smDown`
    margin-bottom: 3rem;
  `}

  ${mediaQueries.mdUp`
    margin: ${(p) => p.theme.tokens.spacing11} 0;
  `}
`;

export const Items = styled.ul`
  padding: 0;
  margin: 0;
  list-style-type: none;
`;

export const Item = styled.li`
  margin: 0;
`;

// Copyright © VNG Realisatie 2019-2024
// Licensed under EUPL v1.2
//
import styled from "styled-components";
import mediaQueries from "@commonground/design-system/dist/mediaQueries";
import { Container } from "src/components/Grid";
import BaseSection from "src/components/Section";

export const Section = styled(BaseSection)`
  padding: 0 0 ${(p) => p.theme.tokens.spacing07};
`;

export const Jumbotron = styled.div`
  position: relative;
  overflow: hidden;
  background: #323232;
  padding-bottom: 20px;

  ${mediaQueries.smUp`
    padding-bottom: 56px;
  `}

  ${mediaQueries.mdUp`
    min-height: 457px;
  `}
`;

export const MobileBottomBg = styled.div`
  ${mediaQueries.smUp`
    display: none;
  `};
`;

export const TextContainer = styled(Container)`
  position: relative;
  z-index: 6;
  overflow: hidden;
  color: ${(p) => p.theme.tokens.colorBackground};
`;

export const Title = styled.h1`
  margin-top: ${(p) => p.theme.tokens.spacing06};

  ${mediaQueries.mdUp`
    margin-top: ${(p) => p.theme.tokens.spacing11};
    max-width: 39rem;
  `}
`;

export const SubTitle = styled.p`
  font-size: ${(p) => p.theme.tokens.fontSizeLarge};
  line-height: 175%;

  ${mediaQueries.mdUp`
    max-width: 27rem;
  `}
`;

export const Background = styled.figure`
  width: 100%;
  height: 100%;
  margin: 0;
  position: absolute;
  overflow: hidden;

  > img {
    height: 100%;
    width: 100%;
    object-fit: cover;
  }
`;

export const Figure = styled.figure`
  position: relative;
  z-index: 5;
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: ${(p) => p.theme.tokens.spacing05};

  ${mediaQueries.smUp`
    flex-direction: column-reverse;
    margin: ${(p) =>
      `-40px ${p.theme.tokens.spacing07} ${p.theme.tokens.spacing09}`};
  `}

  ${mediaQueries.mdUp`
    margin-top: -${(p) => p.theme.tokens.spacing11};
  `}
`;

export const FigCaption = styled.figcaption`
  margin-bottom: ${(p) => p.theme.tokens.spacing05};
  text-align: center;

  ${mediaQueries.smUp`
    margin-top: ${(p) => p.theme.tokens.spacing05};
    margin-bottom: 0;
  `}
`;

export const Video = styled.video`
  width: 390px;
  max-width: 100%;
  height: 220px;
  box-shadow: ${(p) => `rgba(0, 0, 0, 0.24) 0 8px 24px 0`};

  ${mediaQueries.mdUp`
    max-width: unset;
    width: 560px;
    height: 315px;
    box-shadow: ${(p) => `rgba(0, 0, 0, 0.24) 0 24px 40px 0`};
  `}
`;

export const List = styled.ul`
  display: flex;
  align-items: center;
  padding: 0;
  list-style-type: none;
  flex-wrap: wrap;
  justify-content: center;

  > * + * {
    margin-left: ${(p) => `${p.theme.tokens.spacing07}`};
  }

  ${mediaQueries.mdDown`
    > *:first-child, *:last-child {
      width 100%;
      text-align: center;
    }

    > * + * {
    margin: ${(p) => `${p.theme.tokens.spacing07}`} 0 0 0;
    }

    > li {
      min-width: 140px;
      display: flex;
      justify-content: center;
    }
  `}
`;

export const Item = styled.li`
  padding: 0;
`;

export const Image = styled.img`
  display: block;
  filter: grayscale(100%);
  opacity: 0.7;
  max-width: 100px;
  height: 28px;

  ${mediaQueries.smUp`
    height: 28px;
  `}
`;

export const ReferenceText = styled.p`
  margin: 0;
  text-align: right;
  width: 150px;
  flex-shrink: 0;

  ${mediaQueries.mdDown`
    text-align: center;
  `}
`;

export const ReferenceUrl = styled.a`
  text-align: right;
  flex-shrink: 0;
  cursor: pointer;
  text-decoration: underline;
`;
